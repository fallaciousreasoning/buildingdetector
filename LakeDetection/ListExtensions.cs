﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LakeDetection
{
    public static class ListExtensions
    {
        public static List<T> To<T>(this List<T> source, int end)
        {
            var result = new List<T>(end);
            for (var i = 0; i < end; ++i)
                result.Add(source[i]);
            return result;;
        } 

        public static List<T> From<T>(this List<T> source, int start)
        {
            var result = new List<T>();

            for (var i = start; i < source.Count; ++i)
                result.Add(source[i]);   
            
            return result;
        } 
    }
}
