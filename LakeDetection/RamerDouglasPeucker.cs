﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LakeDetection
{
    public class RamerDouglasPeucker
    {
        public static List<Vector> Simplify(List<Vector> input, float epsilon)
        {
            if (input.Count <= 3)
            {
                return input;
            }

            var start = input[0];
            var end = input[input.Count - 1];

            var maxIndex = 0;
            var max = 0f;

            for (var i = 1; i < input.Count - 1; i++)
            {
                var distance = PerpendicularDistance(input[i], start, end);
                if (distance > max)
                {
                    max = distance;
                    maxIndex = i;
                }
            }

            if (max > epsilon)
            {
                var left = input.To(maxIndex);
                var right = input.From(maxIndex);

                left = Simplify(left, epsilon);
                right = Simplify(right, epsilon);

                var result = new List<Vector>();
                result.AddRange(left);
                result.AddRange(right);
                return result;
            }

            return new List<Vector>() {start, end};
        }

        public static float PerpendicularDistance(Vector point, Vector start, Vector end)
        {
            if (start.X == end.X) return Math.Abs(point.X - start.X);

            var slope = (end.Y - start.Y)/(end.X - start.X);
            var intercept = start.Y - slope*start.X;

            var numerator = Math.Abs(slope*point.X - point.Y + intercept);
            var denominator = (float)Math.Sqrt(slope*slope + 1);

            return numerator/denominator;
        }
    }
}
