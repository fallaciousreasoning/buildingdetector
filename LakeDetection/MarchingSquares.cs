﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LakeDetection
{
    public class MarchingSquares
    {
        private Bitmap image;
        private Vector startingPoint;
        private float tolerance;

        private List<Vector> contour; 

        public MarchingSquares(Bitmap image, Vector startingPoint, float tolerance)
        {
            this.image = image;
            this.startingPoint = startingPoint;
            this.tolerance = tolerance;
        }

        public List<Vector> GetContour()
        {
            if (contour != null)
            {
                return contour;
            }

            contour = new List<Vector>();

            var current = startingPoint;
            var step = new Vector();
            var previousStep = new Vector();

            var closed = false;

            while (!closed)
            {
                var squareValue = GetSquareValue(current);
                switch (squareValue)
                {
                    /**
                    |1| |   |1| |   |1| |
                    | | |   |4| |   |4|8|
                    **/
                    case 1:
                    case 5:
                    case 13:
                        step = new Vector(0, -1);
                        break;
                    /**
                    | | |   | |2|   |1|2|
                    | |8|   | |8|   | |8|
                    **/
                    case 8:
                    case 10:
                    case 11:
                        step = new Vector(0, 1);
                        break;
                    /**
                    | | |   | | |   | |2|
                    |4| |   |4|8|   |4|8|
                    **/
                    case 4:
                    case 12:
                    case 14:
                        step = new Vector(-1, 0);
                        break;
                    /**
                    | |2|   |1|2|   |1|2|
                    | | |   | | |   |4| |
                    **/
                    case 2:
                    case 3:
                    case 7:
                        step = new Vector(1, 0);
                        break;
                    /**
                    Saddle case 1
                    | |2|
                    |4| |
                    **/
                    case 6:
                        if (previousStep.X == 0 && previousStep.Y == -1)
                        {
                            step = new Vector(-1, 0);
                        }
                        else
                        {
                            step = new Vector(1, 0);
                        }
                        break;
                    /**
                    |1| |
                    | |8|
                    **/
                    case 9:
                        if (previousStep.X == 1 && previousStep.Y == 0)
                        {
                            step = new Vector(0, -1);
                        }
                        else
                        {
                            step = new Vector(0, 1);
                        }
                        break;
                }
                current += step;
                contour.Add(current);

                previousStep = step;

                if (current == startingPoint)
                    closed = true;
            }

            return contour;
        }

        public Bitmap GetResult(Color color)
        {
            var result = new Bitmap(image.Width, image.Height);

            foreach (var point in contour)
            {
                result.SetPixel((int)point.X, (int)point.Y, color);
            }

            return result;
        }

        private int GetSquareValue(Vector point)
        {
            /*
                Squares are as follows:
                |1|2|
                |4|8|
                where the number indicates the tile is solid and
                can be decomposoded to tell which tiles are solid.
            */
            var squareValue = 0;

            if (SolidAt(point - new Vector(1)))
            {
                squareValue += 1;
            }

            if (SolidAt(point - new Vector(0, 1)))
            {
                squareValue += 2;
            }

            if (SolidAt(point - new Vector(1, 0)))
            {
                squareValue += 4;
            }

            if (SolidAt(point))
            {
                squareValue += 8;
            }

            return squareValue;
        }

        private bool SolidAt(Vector point)
        {
            return image.GetPixel((int) point.X, (int) point.Y).A / 255.0f > (1 - tolerance);
        }
    }
}
