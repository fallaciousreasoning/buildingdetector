﻿using System;

namespace LakeDetection
{
    public struct Vector
    {
        public float X { get; set; }
        public float Y { get; set; }

        public Vector(float n)
        {
            X = n;
            Y = n;
        }

        public Vector(float x, float y)
        {
            X = x;
            Y = y;
        }

        public float LengthSquared()
        {
            return X*X + Y*Y;
        }

        public float Length()
        {
            return (float)Math.Sqrt(LengthSquared());
        }

        public void Normalize()
        {
            var length = Length();
            X /= length;
            Y /= length;
        }

        public float Dot(Vector with)
        {
            return X*with.X + Y*with.Y;
        }

        public float Cross(Vector with)
        {
            return X*with.Y - Y * with.X;
        }

        public static Vector Normalized(Vector vector)
        {
            return vector/vector.Length();
        }

        public static float Distance(Vector a, Vector b)
        {
            return (a - b).Length();
        }

        public bool IsZero()
        {
            return X < float.Epsilon && Y < float.Epsilon;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector)) return false;

            var other = (Vector)obj;
            return other.X == X && other.Y == Y;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode();
        }

        public override string ToString()
        {
            return $"{{X: {X}, Y: {Y}}}";
        }

        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        public static Vector operator *(Vector a, Vector b)
        {
            return new Vector(a.X * b.X, a.Y * b.Y);
        }

        public static Vector operator *(Vector a, float b)
        {
            return new Vector(a.X * b, a.Y * b);
        }

        public static Vector operator /(Vector a, Vector b)
        {
            return new Vector(a.X / b.X, a.Y / b.Y);
        }

        public static Vector operator /(Vector a, float b)
        {
            return new Vector(a.X / b, a.Y / b);
        }

        public static bool operator ==(Vector a, Vector b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Vector a, Vector b)
        {
            return !(a == b);
        }
    }
}
