﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace LakeDetection
{
    public static class ColorExtensions
    {
        private static Random random = new Random();

        public static float Distance(this Color color, Color from)
        {
            var r = Math.Abs(color.R - from.R);
            var g = Math.Abs(color.G - from.G);
            var b = Math.Abs(color.B - from.B);

            return (float) Math.Sqrt(r*r + g*g + b*b);
        }

        public static Color RandomColor()
        {
            return Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
        }
    }
}
