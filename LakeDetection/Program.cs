﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LakeDetection
{
    class Program
    {
        static void Main(string[] args)
        {
            Bitmap sourceImage = new Bitmap(File.OpenRead("a.png"));

            var blobDetector = new BlobDetector(sourceImage, Color.FromArgb(178,213,255), 50);
            var blobs = blobDetector.GetBlobs();

            var blobsImage = blobDetector.GetResult();
            blobsImage.Save("blobs.png");

            var marchingSquares = new MarchingSquares(blobsImage, blobs.First().First(), 0.1f);
            var contour = marchingSquares.GetContour();
            var simplified = RamerDouglasPeucker.Simplify(contour, 0.7f);

            var contourImage = marchingSquares.GetResult(Color.Green);
            contourImage.Save("contour.png");

            Console.WriteLine("Done!");
            Console.ReadKey();
        }
    }
}
