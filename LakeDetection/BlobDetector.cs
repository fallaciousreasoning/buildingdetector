﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LakeDetection
{
    public class BlobDetector
    {
        private Bitmap image;
        private Color blobColor;
        private float threshold;

        private readonly HashSet<Vector> visited = new HashSet<Vector>(); 
        private List<List<Vector>> blobs; 

        public BlobDetector(Bitmap image, Color color, float threshold = 0)
        {
            this.image = image;
            blobColor = color;
            this.threshold = threshold;
        }

        public List<List<Vector>> GetBlobs()
        {
            if (blobs != null)
            {
                return blobs;
            }

            blobs = new List<List<Vector>>();

            for (var x = 0; x < image.Width; ++x)
            {
                for (var y = 0; y < image.Height; ++y)
                {
                    var point = new Vector(x, y);
                    if (visited.Contains(point)) continue;

                    var color = image.GetPixel(x, y);
                    if (color.Distance(blobColor) <= threshold)
                        DFS(point);
                }
            }

            return blobs;
        }

        public Bitmap GetResult()
        {
            var bitmap = new Bitmap(image.Width, image.Height);
            var colors = new [] {Color.Blue, Color.Green};
            var i = 0;

            foreach (var blob in blobs)
            {
                var color = colors[(++i)%colors.Length];
                foreach (var point in blob)
                {
                    var x = (int) point.X;
                    var y = (int) point.Y;

                    bitmap.SetPixel(x, y, color);
                }
            }

            return bitmap;
        }

        private void DFS(Vector start)
        {
            var blob = new List<Vector>();
            var frontier = new Queue<Vector>();

            frontier.Enqueue(start);

            do
            {
                var current = frontier.Dequeue();
                visited.Add(current);
                blob.Add(current);

                GetNeighbours(current).ForEach(frontier.Enqueue);
            } while (frontier.Count > 0);

            blobs.Add(blob);
        }

        private List<Vector> GetNeighbours(Vector point)
        {
            var neighbours = new List<Vector>();
            for (var i = -1; i <= 1; i+=1)
                for (var j = -1; j <= 1; j += 1)
                {
                    var neighbour = new Vector(i, j) + point;

                    if (neighbour.X < 0 || neighbour.Y < 0 || neighbour.X >= image.Width || neighbour.Y >= image.Height) continue;

                    if (visited.Contains(neighbour)) continue;
                    visited.Add(neighbour);

                    var color = image.GetPixel((int)neighbour.X, (int)neighbour.Y);
                    if (color.Distance(blobColor) > threshold) continue;
                    
                    neighbours.Add(neighbour);
                }
            return neighbours;
        } 
    }
}
