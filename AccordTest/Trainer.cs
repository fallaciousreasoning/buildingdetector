﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Accord.Neuro;
using Accord.Neuro.Learning;
using Accord.Neuro.Networks;
using AForge.Math;
using AForge.Neuro.Learning;

namespace AccordTest
{
    public class Trainer
    {
        private string neuralNetName;
        private DeepBeliefNetwork network;

        private DeepBeliefNetworkLearning divergentTeacher;
        private BackPropagationLearning backpropogationTeacher;

        private Tester tester;
        private Random random = new Random();

        public Trainer(string neuralNetName, int inputCount, params int[] layerCounts)
        {
            this.neuralNetName = neuralNetName;

            network = new DeepBeliefNetwork(inputCount, layerCounts);
            new GaussianWeights(network, 0.1).Randomize();
            network.UpdateVisibleWeights();

            divergentTeacher = new DeepBeliefNetworkLearning(network)
            {
                Algorithm = (h, v, i) => new ContrastiveDivergenceLearning(h, v)
                {
                    LearningRate = 0.1,
                    Momentum = 0.5,
                    Decay = 0.001
                }
            };

            backpropogationTeacher = new BackPropagationLearning(network)
            {
                LearningRate = 0.1,
                Momentum = 0.5
            };

            tester = new Tester(neuralNetName, 10, 1.25, 24, 24);
        }

        public void Train()
        {

            var reserveForTesting = 200;
            var samplesPerMultiWindow = 15;

            var positive = PositiveImages().ToList();
            var negative = NegativeImages().ToList();
            var min = Math.Min(positive.Count, negative.Count);
            var take = Math.Max(0, min - reserveForTesting);

            var positiveTraining = positive.Take(take).ToList();
            var positiveTest = positive.Skip(take).ToList();

            var negativeTraining = negative.Take(take).ToList();
            var negativeTest = negative.Skip(take).ToList();

            List<double[]> trainingInputs = new List<double[]>();
            List<double[]> trainingOutputs = new List<double[]>();

            Console.WriteLine("Loading Test Data... ");
            var sum = positiveTraining.Count + negativeTraining.Count;
            var trainingFiles = new List<string>();
            Console.WriteLine($"Loading {negativeTraining.Count * samplesPerMultiWindow + positiveTraining.Count} samples ({negativeTraining.Count*samplesPerMultiWindow} negative; {positiveTraining.Count} positive)");
            for (var i = 0; i < sum; ++i)
            {
                var takePositive = random.NextDouble() > 0.5 && positiveTraining.Count > 0 || negativeTraining.Count == 0;
                var lst = takePositive ? positiveTraining : negativeTraining;
                var index = random.Next(lst.Count);
                var item = lst[index];
                lst.RemoveAt(index);
                trainingFiles.Add(item);

                var bitmap = new Bitmap(File.OpenRead(trainingFiles.Last()));
                var windows = new ImageWindowEnumerator(bitmap, 24, 24, 1.25, bitmap.Width == 24 ? 24 : 50);
                var output = new[] {takePositive ? 1.0 : 0.0};

                for (var j = 0; j < Math.Min(windows.Count, samplesPerMultiWindow); ++j)
                {
                    trainingInputs.Add(windows.RandomFrameUnrepeatable());
                    trainingOutputs.Add(output);
                }
            }

            Console.WriteLine("Loaded!");

            Console.WriteLine("Beginning Training...");

            var inputs = trainingInputs.ToArray();
            var outputs = trainingOutputs.ToArray();
            int batchCount = Math.Max(1, inputs.Length/100);
            int[] groups = Accord.Statistics.Tools.RandomGroups(inputs.Length, batchCount);
            double[][][] batches = inputs.Subgroups(groups);
            double[][][] layerData;

            //Start unsupervised learning
            for (var layerIndex = 0; layerIndex < network.Machines.Count - 1; ++layerIndex)
            {
                divergentTeacher.LayerIndex = layerIndex;
                layerData = divergentTeacher.GetLayerInput(batches);
                for (var i = 0; i < 2500; ++i)
                {
                    var error = divergentTeacher.RunEpoch(layerData)/inputs.Length;
                    if ((i + 1) % 10 == 0) Console.WriteLine($"Error {i + 1}: {error}");
                }
            }

            //Start supervised learning
            for (var i = 0; i < 5000; ++i)
            {
                var error = backpropogationTeacher.RunEpoch(inputs, outputs);
                if ((i + 1) % 10 == 0) Console.WriteLine($"Error {i+1}: {error}");
            }

            Console.WriteLine("Finished Training!");

            Console.WriteLine("Beginning Tests...");

            var n = 0;
            var p = 0;
            var correct = 0;
            var total = 0;
            foreach (var testFile in positiveTest)
            {
                var bitmap = new Bitmap(File.OpenRead(testFile));
                var windows = new ImageWindowEnumerator(bitmap, 24, 10, 1.25);

                var result = network.Compute(windows.RandomFrame());
                correct += result[0] > 0.5 ? 1 : 0;
                total++;
                p++;
            }

            var mul = positiveTest.Count/(float)negativeTest.Count;
            foreach (var testFile in negativeTest)
            {
                var bitmap = new Bitmap(File.OpenRead(testFile));
                var windows = new ImageWindowEnumerator(bitmap, 24, 10, 1.25);

                for (var i = 0; i < Math.Min(Math.Min(mul, windows.Count), windows.Count); ++i)
                {
                    var result = network.Compute(windows.RandomFrameUnrepeatable());
                    correct += result[0] <= 0.5 ? 1 : 0;
                    total++;
                    n++;
                }
            }

            Console.WriteLine($"Finished Testing! Got {correct} out of {total} correct ({(float)correct/total*100}%).");
            Console.WriteLine($"(used {p} positive test windows and {n} negative)");

            Console.WriteLine("Save Result? (y/n)");
            //if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                network.Save(neuralNetName);
            }
        }

        private IEnumerable<string> NegativeImages()
        {
            return File.ReadAllLines("negatives.txt");
        }

        private IEnumerable<string> PositiveImages()
        {
            return File.ReadAllLines("positives.txt");
        } 
    }
}
