﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge;
using ImageProcessor;

namespace AccordTest
{
    public class ImageWindowEnumerator : IEnumerable<double[]>
    {
        private static int id = 0;
        private static readonly Random random  = new Random();

        private readonly int windowSize;
        private readonly int minWindowSize;
        private readonly int step;
        private readonly double scaleStep;
        private readonly Bitmap image;

        private readonly MemoryStream imageMemoryStream;
        private readonly List<Tuple<double, int, int>> frames = new List<Tuple<double, int, int>>();
        private readonly Dictionary<double, byte[]> scaledImages = new Dictionary<double, byte[]>(); 

        public int Count { get { return frames.Count; } }

        public ImageWindowEnumerator(Bitmap image, int windowSize, int step, double scaleStep, int minSize=-1)
        {
            this.image = image;
            this.windowSize = windowSize;
            this.step = step;
            this.scaleStep = scaleStep;
            if (minSize == -1) minSize = windowSize;
            minWindowSize = minSize;

            imageMemoryStream = new MemoryStream();
            image.Save(imageMemoryStream, ImageFormat.Png);

            for (var scale = minWindowSize/(double) windowSize;
                scale*windowSize <= image.Height && scale*windowSize <= image.Width;
                scale *= scaleStep)
            {
                var width = (int) (image.Width/scale);
                var height = (int) (image.Height/scale);
                for (var x = 0; x <= width - windowSize; x += Math.Min(step, width - x - windowSize + 1))
                {
                    for (var y = 0; y <= height - windowSize; y += Math.Min(step, height - y - windowSize + 1))
                    {
                        frames.Add(Tuple.Create(scale, x, y));
                    }
                }
            }
        }

        public IEnumerator<ImageWindow> GetWindowEnumerator()
        {
            return frames.Select(frame => new ImageWindow()
            {
                WindowSize = (int)(frame.Item1*windowSize),
                Data = GetCurrentSquare(frame.Item1, frame.Item2, frame.Item3),
                XOffset = (int)(frame.Item2 * (frame.Item1)),
                YOffset = (int)(frame.Item3 * (frame.Item1)),
            }).GetEnumerator();
        }

        public IEnumerator<double[]> GetEnumerator()
        {
            return frames.Select(frame => GetCurrentSquare(frame.Item1, frame.Item2, frame.Item3)).GetEnumerator();
        }

        public double[] RandomFrame()
        {
            var frame = frames[random.Next(frames.Count)];
            return GetCurrentSquare(frame.Item1, frame.Item2, frame.Item3);
        }

        public double[] RandomFrameUnrepeatable()
        {
            if (frames.Count == 0) throw new Exception("No frames remain!");

            var removeAt = random.Next(frames.Count);
            var frame = frames[removeAt];
            frames.RemoveAt(removeAt);

            return GetCurrentSquare(frame.Item1, frame.Item2, frame.Item3);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private double[] GetCurrentSquare(double scale, int xOffset, int yOffset)
        {
            var width = (int)(image.Width / scale);
            var height = (int)(image.Height / scale);

            if (!scaledImages.ContainsKey(scale)) scaledImages.Add(scale, GetScaledInput(imageMemoryStream, width, height));
            var scaledInput = scaledImages[scale];

            var bitmap = scaledInput.GetBitmapFromGreyScale(width, height);

            var inputStream = new MemoryStream();
            var outputStream = new MemoryStream();
            bitmap.Save(inputStream, ImageFormat.Png);

            using (var factory = new ImageFactory())
            {
                factory.Load(inputStream.ToArray())
                    .Crop(new Rectangle(xOffset, yOffset, windowSize, windowSize))
                    .Save(outputStream);
            }

            bitmap.Dispose();
            bitmap = new Bitmap(outputStream);

            return (from b in bitmap.GetPixelData().ToGreyScale() select b/255.0).ToArray();
        }

        private void SaveSquare(double scale, int xOffset, int yOffset)
        {
            var width = (int) (image.Width/scale);
            var height = (int) (image.Height/scale);

            if (!scaledImages.ContainsKey(scale)) scaledImages.Add(scale, GetScaledInput(imageMemoryStream, width, height));
            var scaledInput = scaledImages[scale];

            var bitmap = scaledInput.GetBitmapFromRgb(width, height);
            var memoryStream = new MemoryStream();
            bitmap.Save(memoryStream, ImageFormat.Png);

            using (var factory = new ImageFactory())
            {
                factory.Load(memoryStream.ToArray())
                    .Crop(new Rectangle(xOffset, yOffset, windowSize, windowSize))
                    .Save(File.Create($"Windows\\{scale}_{xOffset}_{yOffset}.png"));
            }
        }

        private byte[] GetScaledInput(MemoryStream input, int newWidth, int newHeight)
        {
            var memoryStream = new MemoryStream();

            using (var factory = new ImageFactory())
            {
                factory.Load(input.ToArray())
                    .Resize(new Size(newWidth, newHeight))
                    .Save(memoryStream);
            }

            var bitmap = new Bitmap(memoryStream);
            return bitmap.GetPixelData().ToGreyScale();
        }
    }

    public class ImageWindow
    {
        public double[] Data;

        public int XOffset;
        public int YOffset;
        public int WindowSize;
    }
}
