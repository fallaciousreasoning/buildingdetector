﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Accord.Neuro.Networks;
using ImageProcessor;

namespace AccordTest
{
    public class Tester
    {
        private DeepBeliefNetwork network;
        private int step;
        private int windowSize;
        private int minBuildingSize;
        private double scaleStep;

        public Tester(string neuralNetPath, int step = 10, double scaleStep = 1.25, int windowSize = 24,
            int minBuildingSize = 50)
            :this(DeepBeliefNetwork.Load(File.OpenRead(neuralNetPath)), step, scaleStep, windowSize, minBuildingSize)
        {
            
        }

        public Tester(DeepBeliefNetwork network, int step=10, double scaleStep=1.25, int windowSize=24, int minBuildingSize=50)
        {
            if (step <= 0) throw new ArgumentException("Step must be greater than zero!", nameof(step));
            if (windowSize <= 0) throw new ArgumentException("window size must be greater than 0", nameof(windowSize));
            if (scaleStep <= 1) throw new ArgumentException("Scale step must be greater than or equal to 1", nameof(scaleStep));

            this.step = step;
            this.windowSize = windowSize;
            this.scaleStep = scaleStep;
            this.minBuildingSize = minBuildingSize;
            this.network = network;
        }

        public double ContainsBuilding(Bitmap image)
        {
            int x, y, width, height;
            return ContainsBuilding(image, out x, out y, out width, out height);
        }

        public double ContainsBuilding(Bitmap image, out int x, out int y, out int width, out int height)
        {
            var imageWindows = new ImageWindowEnumerator(image, windowSize, step, scaleStep, minBuildingSize);

            x = 0;
            y = 0;
            width = windowSize;
            height = windowSize;

            var max = 0.0;
            var min = 1.0;
            var average = 0.0;
            var count = 0;

            var memoryStream = new MemoryStream();
            image.Save(memoryStream, ImageFormat.Png);

            var enumerator = imageWindows.GetWindowEnumerator();
            while (enumerator.MoveNext())
            {
                var current = enumerator.Current;

                var output = network.Compute(current.Data);
                if (output[0] > max)
                {
                    max = output[0];
                    x = current.XOffset;
                    y = current.YOffset;
                    width = current.WindowSize;
                    height = current.WindowSize;
                }
                if (output[0] < min)
                    min = output[0];

                count++;
                average += output[0];
            }

            average /= count;
            return max;
        }
    }
}
