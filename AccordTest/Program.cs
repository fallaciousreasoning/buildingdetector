﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AccordTest
{
    public class Program
    {
        public static void Main(string[] args)
        {
           // Learner.Learn(args);
           Console.WriteLine("Do you want to train? (y/n)\n");
           if (Console.ReadKey().Key == ConsoleKey.Y) Train(); else Test();
           Console.WriteLine();

            Console.WriteLine("Done");
            Console.ReadKey();
        }

        private static void Train()
        {
            var trainer = new Trainer("buildingdetector.nn", 24*24, 10,10,10, 1);
            trainer.Train();
        }


        private static void Test()
        {
            var picDisplay = new FoundDisplay();

            var tester = new Tester("buildingdetector.nn", 10, 1.25, 24, 100);
            var windowX = 0;
            var windowY = 0;

            foreach (var image in TestImagePaths())
            {
                var bitmap = new Bitmap(image);
                int x, y, width, height;
                var buildingProbability = tester.ContainsBuilding(bitmap, out x, out y, out width, out height);
                Console.WriteLine($"There is a {buildingProbability} probability that image {image} contains a building");

                if (buildingProbability > 0.5)
                {
                    picDisplay.SetImage(bitmap, x, y, width, height);
                    picDisplay.SetDesktopLocation(windowX, windowY);
                    picDisplay.ShowDialog();
                }
            }
            picDisplay.Close();
        }

        private static IEnumerable<string> TestImagePaths()
        {
            return Directory.GetFiles("ManualTest", "*.png");
            return File.ReadAllLines("tests.txt");
        } 
    }
}
