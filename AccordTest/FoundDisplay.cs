﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccordTest
{
    public partial class FoundDisplay : Form
    {
        public FoundDisplay()
        {
            InitializeComponent();
            KeyPress += (sender, args) =>
            {
                if (args.KeyChar == ' ') Close();
            };
            Shown += (sender, args) => Focus();
        }

        public void SetImage(Bitmap image, int startX, int startY, int width, int height)
        {
            foundPic.Image = image;

            using (var graphics = Graphics.FromImage(foundPic.Image))
            {
                Rectangle rect = new Rectangle(startX, startY, width, height);
                graphics.DrawRectangle(Pens.Red, rect);
            }
            foundPic.Invalidate();
        }
    }
}
