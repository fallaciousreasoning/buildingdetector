﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Accord.Math;
using Accord.Neuro;
using Accord.Neuro.Learning;
using Accord.Neuro.Networks;
using AForge.Math;
using AForge.Neuro.Learning;

namespace AccordTest
{
    public class Learner
    {
        public static void Learn(string[] args)
        {
            double[][] inputs;
            double[][] outputs;
            double[][] testInputs;
            double[][] testOutputs;

            Load(out inputs, out outputs);

            var test = 1000;
            testInputs = inputs.Skip(test).ToArray();
            testOutputs = outputs.Skip(test).ToArray();

            inputs = inputs.Take(test).ToArray();
            outputs = outputs.Take(test).ToArray();

            DeepBeliefNetwork network;
            network = new DeepBeliefNetwork(inputs.First().Length, 10, 10, 10, 1);
            new GaussianWeights(network, 0.1).Randomize();
            network.UpdateVisibleWeights();

            var teacher = new DeepBeliefNetworkLearning(network)
            {
                Algorithm = (h, v, i) => new ContrastiveDivergenceLearning(h, v)
                {
                    LearningRate = 0.1,
                    Momentum = 0.5,
                    Decay = 0.001,
                }
            };

            int batchCount = Math.Max(1, inputs.Length/100);
            int[] groups = Accord.Statistics.Tools.RandomGroups(inputs.Length, batchCount);

            double[][][] batches = inputs.Subgroups(groups);
            // Learning data for the specified layer.
            double[][][] layerData;

            // Unsupervised learning on each hidden layer, except for the output layer.
            for (int layerIndex = 0; layerIndex < network.Machines.Count - 1; layerIndex++)
            {
                teacher.LayerIndex = layerIndex;
                layerData = teacher.GetLayerInput(batches);
                for (int i = 0; i < 200; i++)
                {
                    double error = teacher.RunEpoch(layerData)/inputs.Length;
                    if (i%10 == 0)
                    {
                        Console.WriteLine(i + ", Error = " + error);
                    }
                }
            }

            // Supervised learning on entire network, to provide output classification.
            var teacher2 = new BackPropagationLearning(network)
            {
                LearningRate = 0.1,
                Momentum = 0.5
            };

            // Run supervised learning.
            for (int i = 0; i < 500; i++)
            {
                double error = teacher2.RunEpoch(inputs, outputs)/inputs.Length;
                if (i%10 == 0)
                {
                    Console.WriteLine(i + ", Error = " + error);
                }
            }


            // Test the resulting accuracy.
            int correct = 0;
            for (int i = 0; i < inputs.Length; i++)
            {
                double[] outputValues = network.Compute(testInputs[i]);
                if (outputValues[0] > 0.5 && testOutputs[i][0] == 1 || outputValues[0] <= 0.5 && testOutputs[i][0] == 0)
                {
                    correct++;
                }
            }

            using (var stream = File.Create("buildingdetector.nn"))
            {
                network.Save(stream);
            }

            Console.WriteLine("Correct " + correct + "/" + inputs.Length + ", " +
                              Math.Round(((double) correct/(double) inputs.Length*100), 2) + "%");
        }

        private static void Load(out double[][] inputs, out double[][] outputs)
        {
            var positiveFiles = File.ReadAllLines("positives.txt");
            var negativeFiles = File.ReadAllLines("negatives.txt");

            var allFiles = new List<string>();
            allFiles.AddRange(negativeFiles.Take(250));
            allFiles.AddRange(positiveFiles);
            allFiles.AddRange(negativeFiles.Skip(250));

            inputs = new double[allFiles.Count][];
            outputs = new double[allFiles.Count][];

            for (var i = 0; i < allFiles.Count; ++i)
            {
                var file = allFiles[i];
                inputs[i] = LoadImage(file);
                outputs[i] = new [] { file.Contains("Positive") ? 1.0 : 0.0 };
            }
        }

        private static double[] LoadImage(string filename)
        {
            var bitmap = new Bitmap(filename);

            var data = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            var result = new byte[24*24*3];
            Marshal.Copy(data.Scan0, result, 0, result.Length);
            bitmap.UnlockBits(data);

            bitmap.Dispose();

            return (from b in result select (double) b/255).ToArray();
        }
    }
}
