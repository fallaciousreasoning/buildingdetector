﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AccordTest
{
    public static class BitmapExtensions
    {
        public static byte[] GetPixelData(this Bitmap bitmap)
        {
            var data = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            var result = new byte[bitmap.Width * bitmap.Height * 3];
            Marshal.Copy(data.Scan0, result, 0, result.Length);
            bitmap.UnlockBits(data);

            return result;
        }

        public static Bitmap GetBitmapFromRgb(this byte[] pixelData, int width, int height)
        {
            var bitmap = new Bitmap(width, height);
            var data = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.ReadWrite,
                PixelFormat.Format24bppRgb);

            Marshal.Copy(pixelData, 0, data.Scan0 , pixelData.Length);
            bitmap.UnlockBits(data);

            return bitmap;
        }

        public static Bitmap GetBitmapFromGreyScale(this byte[] greyScalePixels, int width, int height)
        {
            var pixelData = new byte[greyScalePixels.Length*3];
            for (var i = 0; i < greyScalePixels.Length; ++i)
            {
                var index = i*3;
                pixelData[index] = greyScalePixels[i];
                pixelData[index+1] = greyScalePixels[i];
                pixelData[index+2] = greyScalePixels[i];
            }
            return pixelData.GetBitmapFromRgb(width, height);
        }

        public static byte[] ToGreyScale(this byte[] rgb)
        {
            var result = new byte[rgb.Length/3];
            for (var i = 0; i < result.Length; ++i)
            {
                var index = i*3;
                var b = rgb[index];
                var g = rgb[index + 1];
                var r = rgb[index + 2];

                var average = (byte) ((r + g + b)/3);
                result[i] = average;
            }
            return result;
        }
    }
}
