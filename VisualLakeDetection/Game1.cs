﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PolygonMerging;
using VisualLakeDetection.Detection;
using VisualLakeDetection.Detection.Edges;
using VisualLakeDetection.Detection.Segmenting;

namespace VisualLakeDetection
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        PrimitiveBatch primitiveBatch;

        Matrix view = Matrix.Identity;
        Matrix projection = Matrix.Identity;

        string textureName = "";

        Texture2D baseTexture;
        Texture2D blobTexture;
        Texture2D contourTexture;

        SpriteFont font;
        
        List<List<Vector2>> contours = new List<List<Vector2>>();
        List<List<Vector2>> simplifiedContours = new List<List<Vector2>>();

        private bool displayOriginal = true;
        private bool displayBlob = true;
        private bool displayContour = true;
        private bool displayPolygon = true;

        private float simplification = 5f;
        private float simplificationStep = 0.05f;

        private KeyboardState oldKeyState;
        private KeyboardState keyState;

        private string helpfulInfo = "";

        public Game1(string textureName)
        {
            this.textureName = textureName;
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            IsMouseVisible = true;
            // TODO: Add your initialization logic here
            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4, GraphicsDevice.Viewport.AspectRatio,
                0.1f, 10000f);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            primitiveBatch = new PrimitiveBatch(GraphicsDevice);

            font = Content.Load<SpriteFont>("Font");

            if (!string.IsNullOrEmpty(textureName))
            {
                baseTexture = Texture2D.FromStream(GraphicsDevice, File.OpenRead(textureName));
            }

            var baseColors = new Color[baseTexture.Width * baseTexture.Height];
            baseTexture.GetData(baseColors);

            var edgeDetector = new DerivationEdgeDetector(0.9f);
            var edges = edgeDetector.Calculate(baseColors, baseTexture.Width, baseTexture.Height);
            //baseColors = edgeDetector.Render(baseColors, edges);
            
            //baseTexture.SetData(baseColors);

            var mapWaterColor = new Color(178, 213, 255);
            var poolWaterColor = new Color(66,148,158);

            var regionDetector = new Watershed(baseColors, baseTexture.Width, baseTexture.Height);
            var regions = regionDetector.Segment();
            var regionData = regionDetector.GetResult();

//            var blobDetector = new BlobDetector(baseColors, baseTexture.Width, baseTexture.Height,new Color(0, 255, 0), 70);
//            var blobs = blobDetector.GetBlobs().Where(b => b.Count >= 50);
//
//            var blobData = blobDetector.GetResult(Color.Blue);
            blobTexture = new Texture2D(GraphicsDevice, baseTexture.Width, baseTexture.Height);
            blobTexture.SetData(regionData);
            blobTexture.SaveAsPng(File.Create("blob.png"), blobTexture.Width, blobTexture.Height);
//
            var contourData = new Color[blobTexture.Width*blobTexture.Height];

            foreach (var region in regions)
            {
                if (region.Count < 100) continue;
                var min = new Vector2(blobTexture.Height);
                foreach (var point in region)
                {
                    if (point.Y < min.Y)
                    {
                        min = point;
                    }
                    if (point.Y == min.Y && point.X < min.X) min = point;
                }
                var contourDetector = new MarchingSquares(regionData, blobTexture.Width, blobTexture.Height, min, 0.1f);
                var contour = contourDetector.GetContour();

                contours.Add(contour);
                contourData = contourDetector.GetResult(contourData, Color.Green);
            }

            contourTexture = new Texture2D(GraphicsDevice, blobTexture.Width, blobTexture.Height);
            contourTexture.SetData(contourData);
            contourTexture.SaveAsPng(File.Create("contour.png"), contourTexture.Width, contourTexture.Height);

            ReSimplify();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            keyState = Keyboard.GetState();

            if (Pressed(Keys.O)) displayOriginal = !displayOriginal;
            if (Pressed(Keys.B)) displayBlob = !displayBlob;
            if (Pressed(Keys.C)) displayContour = !displayContour;
            if (Pressed(Keys.P)) displayPolygon = !displayPolygon;

            if (keyState.IsKeyDown(Keys.Up))
            {
                simplification += simplificationStep;
            }
            if (keyState.IsKeyDown(Keys.Down))
            {
                simplification -= simplificationStep;
            }

            simplification = MathHelper.Clamp(0, simplification, 5f);
            //ReSimplify();

            oldKeyState = keyState;

//            var m = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
//            var distMin = -1f;
//            var closest = -1;
//
//            for (int i = 0; i < centroids.Count; i++)
//            {
//                var dist = Vector2.Distance(m, centroids[i]);
//                if (dist < distMin || closest == -1)
//                {
//                    distMin = dist;
//                    closest = i;
//                }
//            }
//
//            if (closest != -1)
//            {
//
//                var area = 0f;
//                for (var i = 0; i < contours[closest].Count; ++i)
//                {
//                    area += contours[closest][i].X*contours[closest][(i + 1)%contours[closest].Count].Y -
//                            contours[closest][i].Y*contours[closest][(i + 1)%contours[closest].Count].X;
//                }
//                area = Math.Abs(area/2);
//
//                helpfulInfo =
//                    $"Perimeter: {contours[closest].Count}, Area: {area}, Ratio: {area/contours[closest].Count}";
//            }

            base.Update(gameTime);
        }

        private void ReSimplify()
        {
            simplifiedContours.Clear();
            foreach (var contour in contours)
            {
                simplifiedContours.Add(RamerDouglasPeucker.Simplify(contour, simplification));
            }
        }

        private bool Pressed(Keys key)
        {
            return !oldKeyState.IsKeyDown(key) && keyState.IsKeyDown(key);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            if (displayOriginal && baseTexture != null)
                spriteBatch.Draw(baseTexture, Vector2.Zero, Color.White);

            if (displayBlob && blobTexture != null)
            {
                spriteBatch.Draw(blobTexture, Vector2.Zero, Color.White);
            }

            if (displayContour && contourTexture != null)
            {
                spriteBatch.Draw(contourTexture, Vector2.Zero, Color.White);
            }

            spriteBatch.End();

            if (displayPolygon)
            {
                foreach (var simplifiedContour in simplifiedContours)
                {
                    primitiveBatch.Begin(PrimitiveType.TriangleList);

                    foreach (var point in simplifiedContour)
                    {
                        var halfSize = new Vector2(3);
                        var color = Color.White;

                        primitiveBatch.DrawTriangle(point - halfSize, point + new Vector2(halfSize.X, -halfSize.Y),
                            point + halfSize, color);
                        primitiveBatch.DrawTriangle(point - halfSize, point + halfSize,
                            point + new Vector2(-halfSize.X, halfSize.Y), color);
                    }

                    primitiveBatch.End();

                    primitiveBatch.Begin(PrimitiveType.LineList);

                    for (var i = 0; i < simplifiedContour.Count - 1; ++i)
                    {
                        primitiveBatch.DrawLine(simplifiedContour[i], simplifiedContour[i + 1], Color.Red);
                    }

                    primitiveBatch.End();
                }
            }

            DrawGUI();

            base.Draw(gameTime);
        }

        private void DrawGUI()
        {
            var lines = new List<string>()
            {
                $"Display Base Layer: {displayOriginal} (toggle O)",
                $"Display Blob Layer: {displayBlob} (toggle B)",
                $"Display Contour Layer: {displayContour} (toggle C)",
                $"Display Polgony: {displayPolygon} (toggle P)",
                $"Increase/decrease simplification with Up and Down (currently {simplification})",
            };
            
            spriteBatch.Begin();

            for (var i = 0; i < lines.Count; ++i)
                spriteBatch.DrawString(font, lines[i], new Vector2(20, 10 + i * (font.MeasureString(lines[i]).Y + 5)), Color.Black);

            spriteBatch.DrawString(font, helpfulInfo, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);

            spriteBatch.End();
        }
    }
}
