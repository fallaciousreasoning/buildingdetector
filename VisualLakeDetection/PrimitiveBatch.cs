﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PolygonMerging
{
    public class PrimitiveBatch
    {
        private const int VERTEX_BUFFER_SIZE = 500;
        private const int VERTS_PER_LINE = 2;
        private const int VERTS_PER_TRIANGLE = 3;

        private readonly VertexPositionColor[] vertexBuffer = new VertexPositionColor[VERTEX_BUFFER_SIZE];

        private readonly GraphicsDevice device;
        private bool hasBegun;

        private PrimitiveType currentType;
        private int currentIndex;

        private BasicEffect effect;

        /// <summary>
        /// The number of vertices per primitive
        /// </summary>
        private int VerticesPerPrimitivew
        {
            get { return currentType == PrimitiveType.LineList ? VERTS_PER_LINE : VERTS_PER_TRIANGLE; }
        }

        public PrimitiveBatch(GraphicsDevice device, BasicEffect effect=null)
        {
            this.device = device;
            this.effect = effect ?? new BasicEffect(device)
            {
                Projection = Matrix.CreateOrthographicOffCenter(0, device.Viewport.Width, device.Viewport.Height, 0,
                    0, 1),
                VertexColorEnabled = true
            };
        }

        public void Begin(PrimitiveType type)
        {
            if (hasBegun) throw new Exception("End must be called before you can begin");

            currentType = type;
            hasBegun = true;
        }

        public void Begin(PrimitiveType type, Matrix world)
        {
            effect.World = world;

            Begin(type);
        }

        public void DrawTriangle(Vector2 a, Vector2 b, Vector2 c, Color color)
        {
            DrawTriangle(new Vector3(a, 0), new Vector3(b, 0), new Vector3(c, 0), color);
        }

        public void DrawTriangle(Vector3 a, Vector3 b, Vector3 c, Color color)
        {
            AddVertex(a, color);
            AddVertex(b, color);
            AddVertex(c, color);
        }

        public void DrawLine(Vector2 start, Vector2 end, Color color)
        {
            DrawLine(new Vector3(start, 0), new Vector3(end, 0), color);
        }

        public void DrawLine(Vector3 start, Vector3 end, Color color)
        {
            AddVertex(start, color);
            AddVertex(end, color);
        }

        public void AddVertex(Vector3 vertex, Color color)
        {
            if (currentIndex%VerticesPerPrimitivew == 0 && currentIndex + VerticesPerPrimitivew >= vertexBuffer.Length)
            {
                Flush();
            }

            vertexBuffer[currentIndex++] = new VertexPositionColor(vertex, color);
        }

        public void End()
        {
            if (!hasBegun) throw new Exception("You must call 'Begin' before end can be called");

            Flush();
            hasBegun = false;
        }

        private void Flush()
        {
            if (currentIndex % VerticesPerPrimitivew != 0) throw new Exception($"You must have a multiple of {VerticesPerPrimitivew} vertices when drawing {currentType}s");

            foreach (var pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();
            }

            if (currentIndex == 0) return;

            device.DrawUserPrimitives(currentType, vertexBuffer, 0, currentIndex/VerticesPerPrimitivew);
            currentIndex = 0;
        }
    }
}
