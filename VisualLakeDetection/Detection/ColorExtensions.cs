﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection
{
    public static class ColorExtensions
    {
        private static Random random = new Random();

        public static Color At(this Color[] source, Vector2 point, int width)
        {
            return source.At((int) point.X, (int) point.Y, width);
        }

        public static Color At(this Color[] source, int x, int y, int width)
        {
            return source[x + y*width];
        }

        public static float Distance(this Color color, Color from)
        {
            var r = Math.Abs(color.R - from.R);
            var g = Math.Abs(color.G - from.G);
            var b = Math.Abs(color.B - from.B);

            return (float) Math.Sqrt(r*r + g*g + b*b);
        }

        public static Color ToGreyScale(this Color color)
        {
            var average = (color.R + color.G + color.B) / 3;
            return new Color(average, average, average);
        }

        public static Color RandomColor()
        {
            return new Color(random.Next(256), random.Next(256), random.Next(256));
        }

        public static Color[] RGBify(Color[] colors)
        {
            var result = new Color[colors.Length];

            for (int i = 0; i < colors.Length; i++)
            {
                var color = colors[i];
                var max = Max(color.R, color.G, color.B);
                if (color.R == max) result[i] = Color.Red;
                if (color.G == max) result[i] = new Color(0, 255, 0);
                if (color.B == max) result[i] = new Color(0, 0, 255);
            }
            return result;
        }

        public static Color[] Greyify(Color[] colors)
        {
            var result = new Color[colors.Length];

            for (var i = 0; i < colors.Length; ++i)
            {
                var color = colors[i];
                var average = (color.R + color.G + color.B)/3;
                var newColor = new Color(average, average, average);
                result[i] = newColor;
            }
            return result;
        }

        public static Color[] Brutalize(Color[] colors)
        {
            var result = new Color[colors.Length];

            for (var i = 0; i < colors.Length; ++i)
            {
                var color = colors[i];
                color.R = (byte)(color.R * 0.299f);
                color.G = (byte)(color.G * 0.587f);
                color.B = (byte)(color.B * 0.114f);
                //0,299.R + 0,587.G + 0,114.B
                //var newColor = new Color(0.299f*color.R, 0.587f*color.G, 0.114f*color.B);
                result[i] = color;
            }
            return result;
        }

        public static Color Average(Color original, int weight, Color with)
        {
            float r = original.R*weight + with.R;
            float g = original.G * weight + with.G;
            float b = original.B * weight + with.G;

            var i = weight + 1;
            return new Color((byte)(r / i), (byte)(g / i), (byte)(b / i));
        }

        public static Color Average(IEnumerable<Color> colors)
        {
            float r = 0, g = 0, b = 0;
            var i = 0;
            foreach (var color in colors)
            {
                r += color.R;
                g += color.G;
                b += color.B;
                i++;
            }

            return new Color((byte)(r / i), (byte)(g / i), (byte)(b / i));
        }

        public static Color Average(params Color[] colors)
        {
            return Average(colors.ToList());
        }

        private static int Max(params byte[] input)
        {
            var max = 0;
            foreach (var b in input)
                if (b > max) max = b;
            return max;
        }
    }
}
