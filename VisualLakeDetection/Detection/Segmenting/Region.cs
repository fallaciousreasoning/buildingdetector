﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Segmenting
{
    public class Region
    {
        public Dictionary<Vector2, Color> Colors; 
        public List<Vector2> Points;

        public Color AverageColor;
        public Vector2 Min;
        public Vector2 Max;
        public Vector2 Centroid;

        public Region()
        {
            Points = new List<Vector2>();
            Colors = new Dictionary<Vector2, Color>();
        }

        public void Add(Vector2 point, Color color)
        {
            AverageColor = ColorExtensions.Average(AverageColor, Points.Count, color);
            Centroid = (Centroid*Points.Count + point)/(Points.Count + 1);

            Points.Add(point);
            Colors.Add(point, color);

            if (Min.X > point.X) Min.X = point.X;
            if (Min.Y > point.Y) Min.Y = point.Y;
            if (Max.X < point.X) Max.X = point.X;
            if (Max.Y < point.Y) Max.Y = point.Y;
        }

        public Color this[Vector2 point]
        {
            get { return Colors[point]; }
        }

        public Vector2 this[int point]
        {
            get { return Points[point]; }
        }
    }
}
