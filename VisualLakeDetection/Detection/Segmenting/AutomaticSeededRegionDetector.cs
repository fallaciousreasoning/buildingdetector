﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection
{
    public class AutomaticSeededRegionDetector
    {
        private List<List<Vector2>> regions;
         
        private Color[] imageData;
        private int width;
        private int height;

        private float threshold;
        private int rasterSize;

        public AutomaticSeededRegionDetector(Color[] imageData, int width, int height, float threshold, int rasterSize)
        {
            this.imageData = imageData;
            this.width = width;
            this.height = height;
            this.threshold = threshold;
            this.rasterSize = rasterSize;
        }

        public List<List<Vector2>> GetRegions()
        {
            if (this.regions != null) return this.regions;

            var allowedSeeds = new HashSet<Vector2>();
            var seeds = new Stack<Vector2>();
            var visited = new HashSet<Vector2>();

            for (var i = 0; i < width; i += rasterSize)
            {
                for (var j = 0; j < height; j += rasterSize)
                {
                    var seed = new Vector2(i, j);
                    seeds.Push(seed);
                    allowedSeeds.Add(seed);
                }
            }

            var regions = new List<List<Vector2>>();

            while (seeds.Count > 0)
            {
                var seed = seeds.Pop();
                if (!allowedSeeds.Contains(seed)) continue;

                var frontier = new Stack<Vector2>();
                var region = new List<Vector2>();

                var color = At(seed);
                if (color == Color.Black) continue;

                frontier.Push(seed);


                while (frontier.Count > 0)
                {
                    var current = frontier.Pop();
                    if (visited.Contains(current)) continue;

                    visited.Add(current);
                    region.Add(current);

                    if (allowedSeeds.Contains(current)) allowedSeeds.Remove(current);

                    var neighbours = Neighbours(current);
                    foreach (var neighbour in neighbours)
                    {
                        if (visited.Contains(neighbour)) continue;
                        var nColor = At(neighbour);
                        if (color.Distance(nColor)/255f <= threshold)
                        {
                            frontier.Push(neighbour);
                            color = ColorExtensions.Average(color, visited.Count, nColor);
                        }
                    }
                }

                regions.Add(region);
            }

            this.regions = regions;
            return regions;
        }

        private List<Vector2> Neighbours(Vector2 point)
        {
            var neighbours = new List<Vector2>();

            for (var i = -1; i <= 1; ++i)
                for (var j = -1; j <= 1; ++j)
                {
                    var n = point + new Vector2(i, j);
                    if (i == 0 && j == 0) continue;
                    if (n.X < 0 || n.Y < 0 || n.X >= width || n.Y >= height) continue;
                    neighbours.Add(n);
                }
            return neighbours;
        }

        private int Index(Vector2 point)
        {
            return (int)(point.X + point.Y * width);
        }

        private Color At(Vector2 point)
        {
            return imageData[Index(point)];
        }

        public Color[] GetResult()
        {
            var colors = new Color[imageData.Length];

            foreach (var region in regions)
            {
                var color = region.Count < 100 ? Color.Transparent : At(region.First());//ColorExtensions.RandomColor();
                foreach (var point in region)
                {
                    colors[Index(point)] = color;
                }
            }

            return colors;
        }
    }
}
