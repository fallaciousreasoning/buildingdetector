﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Segmenting
{
    public class SplitMergeRegionDetector
    {
        private Color[] imageData;
        private int width;
        private int height;

        private List<List<Vector2>> regions;

        private int rasterSize;
        private float tolerance;

        public SplitMergeRegionDetector(Color[] imageData, int width, int height, int rasterSize = 10, float tolerance = 0.3f)
        {
            this.imageData = imageData;
            this.width = width;
            this.height = height;
            this.rasterSize = rasterSize;
            this.tolerance = tolerance;
        }


        public List<List<Vector2>> GetRegions()
        {
            var allocation = new int[width, height];
            var seeds = new Stack<Vector2>();
            for (var i = 0; i < width; i += rasterSize)
            {
                for (var j = 0; j < height; j += rasterSize)
                {
                    var seed = new Vector2(i, j);
                    seeds.Push(seed);
                }
            }

            var regions = new List<Region>();

            do
            {
                var seed = seeds.Pop();
                var region = new Region();

                var frontier = new Stack<Vector2>();
                frontier.Push(seed);

                do
                {
                    var currentPoint = frontier.Pop();
                    var currentColor = At(currentPoint);

                    var x = (int)currentPoint.X;
                    var y = (int)currentPoint.Y;
                    if (allocation[x, y] != 0) continue;

                    allocation[x, y] = regions.Count + 1;
                    region.Add(currentPoint, currentColor);

                    var neighbours = Neighbours(currentPoint);

                    foreach (var neighbour in neighbours)
                    {
                        if (allocation[(int)neighbour.X, (int)neighbour.Y] != 0) continue;
                        var nColor = At(neighbour);

                        if (region.AverageColor.Distance(nColor) / 255f <= tolerance) frontier.Push(neighbour);
                    }

                } while (frontier.Count > 0);

                if (region.Points.Count > 0)
                {
                    regions.Add(region);
                }
            } while (seeds.Count > 0);

            var asV2 = (from region in regions select region.Points).ToList();
            this.regions = asV2;
            return asV2;
        }

        private int Index(Vector2 point)
        {
            return (int)(point.X + point.Y * width);
        }

        private Color At(Vector2 point)
        {
            return imageData[Index(point)];
        }
        private List<Vector2> Neighbours(Vector2 point)
        {
            var neighbours = new List<Vector2>();

            for (var i = -1; i <= 1; ++i)
                for (var j = -1; j <= 1; ++j)
                {
                    var n = point + new Vector2(i, j);
                    if (i == 0 && j == 0) continue;
                    if (n.X < 0 || n.Y < 0 || n.X >= width || n.Y >= height) continue;
                    neighbours.Add(n);
                }
            return neighbours;
        }

        public Color[] GetResult()
        {
            var colors = new Color[imageData.Length];

            foreach (var region in regions)
            {
                var color = region.Count < 100 ? Color.Transparent : At(region.First());//ColorExtensions.RandomColor();
                foreach (var point in region)
                {
                    colors[Index(point)] = color;
                }
            }

            return colors;
        }
    }
}
