﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Segmenting.Morphology
{
    public class Closer : IDetectionStep<List<List<Vector2>>>
    {
        private float closeRadius; 
        private int width;

        private List<List<Vector2>> baseRegions; 

        public Closer(float radius, List<List<Vector2>> baseRegions)
        {
            closeRadius = radius;

            this.baseRegions = baseRegions.Where(r => r.Count >= 100 && r.Count <= 7500).ToList();
            this.baseRegions.Sort((r1, r2) => Comparer<int>.Default.Compare(r1.Count, r2.Count));
        }

        public List<List<Vector2>> Calculate(Color[] imageData, int width, int height)
        {
            this.width = width;
            
            var result = new List<List<Vector2>>();

            return null;
        }

        private void FindMinMax(List<Vector2> source, out Vector2 min, out Vector2 max)
        {
            min = new Vector2(-1);
            max = new Vector2(-1);

            foreach (var vector2 in source)
            {
                if (max == new Vector2(-1))
                {
                    min = vector2;
                    max = vector2;
                }

                if (min.X > vector2.X) min.X = vector2.X;
                if (min.Y > vector2.Y) min.Y = vector2.Y;
                if (max.X < vector2.X) max.X = vector2.X;
                if (max.Y < vector2.Y) max.Y = vector2.Y;
            }
        }

        public Color[] Render(Color[] original, List<List<Vector2>> result)
        {
            foreach (var region in result)
            {
                var color = region.Count < 100 || region.Count > 5000 ? Color.Transparent : ColorExtensions.RandomColor();
                foreach (var point in region)
                {
                    original[(int)(point.X + point.Y * width)] = color;
                }
            }

            return original;
        }
    }
}
