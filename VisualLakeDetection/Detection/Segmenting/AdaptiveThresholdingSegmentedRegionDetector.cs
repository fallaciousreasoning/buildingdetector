﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection
{
    public class AdaptiveThresholdingSegmentedRegionDetector
    {
        private List<List<Vector2>> regions;
         
        private Color[] imageData;
        private int width;
        private int height;

        private float threshold;

        private HashSet<Vector2> visited = new HashSet<Vector2>();
        private int[] histogram = new int[256]; 

        public AdaptiveThresholdingSegmentedRegionDetector(Color[] imageData, int width, int height)
        {
            this.imageData = imageData;
            this.width = width;
            this.height = height;
        }

        public List<List<Vector2>> GetRegions()
        {
            if (this.regions != null)
            {
                return this.regions;
            }

            CalculateHistogram();
            threshold = Otsu();

            var regions = new List<List<Vector2>>();
            var boundaryPixels = new Stack<Vector2>();

            var seed = new Vector2(width / 2, height / 2);

            do
            {
                var region = GetRegion(seed, boundaryPixels);
                regions.Add(region);

                seed = boundaryPixels.Pop();
            } while (boundaryPixels.Count > 0);

            this.regions = regions;
            return regions;
        }

        private List<Vector2> GetRegion(Vector2 seed, Stack<Vector2> boundaryPixels)
        {
            var region = new List<Vector2>();
            var pixels = new Stack<Vector2>();
            var seedColor = At(seed);

            pixels.Push(seed);

            while (pixels.Count > 0)
            {
                var currentPixel = pixels.Pop();
                region.Add(currentPixel);

                var neighbours = Neighbours(currentPixel);
                foreach (var neighbour in neighbours)
                {
                    if (!visited.Contains(neighbour))
                    {
                        var color = At(neighbour);
                        var distance = seedColor.Distance(color);

                        if (distance <= 10)
                        {
                            visited.Add(neighbour);
                            pixels.Push(neighbour);
                        }
                        else
                        {
                            boundaryPixels.Push(neighbour);
                        }
                    }
                }
            }

            return region;
        }

        private List<Vector2> Neighbours(Vector2 point)
        {
            var neighbours = new List<Vector2>();

            for (var i = -1; i <=1; ++i)
                for (var j = -1; j <= 1; ++j)
                {
                    var n = point + new Vector2(i, j);
                    if (n.X < 0 || n.Y < 0 ||n.X >= width || n.Y >= height) continue;
                    neighbours.Add(n);
                }
            return neighbours;
        }

        private int Index(Vector2 point)
        {
            return (int) (point.X + point.Y*width);
        }

        private Color At(Vector2 point)
        {
            return imageData[Index(point)];
        }

        private float Otsu()
        {
            var sum = 0;
            for (var i = 0; i < histogram.Length; ++i)
                sum += i * histogram[i];

            var sumB = 0;
            var wb = 0;
            var wf = 0;
            var mb = 0f;
            var mf = 0f;
            var max = 0f;
            var between = 0f;
            var threshold1 = 0f;
            var threshold2 = 0f;

            for (var i = 0; i < histogram.Length; ++i)
            {
                wb += histogram[i];
                if (wb == 0) continue;

                wf = (width*height) - wb;
                if (wf == 0) break;

                sumB += i*histogram[i];
                mb = sumB/(float)wb;
                mf = (sum - sumB)/(float)wf;
                between = wb*wf*(mb - mf)*(mb - mf);
                if (between >= max)
                {
                    threshold1 = i;
                    if (between > max)
                    {
                        threshold2 = i;
                    }
                    max = between;
                }
            }
            return (threshold1 + threshold2)/2f;
        }

        private void CalculateHistogram()
        {
            foreach (var color in imageData)
            {
                var intensity = (color.R + color.G + color.B) / 3;
                histogram[intensity]++;
            }
        }

        public Color[] GetResult()
        {
            var colors = new Color[imageData.Length];

            foreach (var region in regions)
            {
                var color = ColorExtensions.RandomColor();
                foreach (var point in region)
                {
                    colors[Index(point)] = color;
                }
            }

            return colors;
        }
    }
}
