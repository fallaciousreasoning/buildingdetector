﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection
{
    public class BlobDetector
    {
        private Color[] image;
        private int width;
        private int height;

        private Color blobColor;
        private float threshold;

        private readonly HashSet<Vector2> visited = new HashSet<Vector2>(); 
        private List<List<Vector2>> blobs; 

        public BlobDetector(Color[] image, int width, int height, Color color, float threshold = 0)
        {
            this.image = image;
            this.width = width;
            this.height = height;

            blobColor = color;
            this.threshold = threshold;
        }

        public List<List<Vector2>> GetBlobs()
        {
            if (blobs != null)
            {
                return blobs;
            }

            blobs = new List<List<Vector2>>();

            for (var x = 0; x < width; ++x)
            {
                for (var y = 0; y < height; ++y)
                {
                    var point = new Vector2(x, y);
                    if (visited.Contains(point)) continue;

                    var color = image[x + y * width];
                    if (color.Distance(blobColor) <= threshold)
                        DFS(point);
                }
            }

            return blobs;
        }

        public Color[] GetResult(Color color)
        {
            var bitmap = new Color[width*height];
            var i = 0;

            foreach (var blob in blobs)
            {
                foreach (var point in blob)
                {
                    var x = (int) point.X;
                    var y = (int) point.Y;

                    bitmap[x + y * width] = color;
                }
            }

            return bitmap;
        }

        private void DFS(Vector2 start)
        {
            var blob = new List<Vector2>();
            var frontier = new Queue<Vector2>();

            frontier.Enqueue(start);

            do
            {
                var current = frontier.Dequeue();
                visited.Add(current);
                blob.Add(current);

                GetNeighbours(current).ForEach(frontier.Enqueue);
            } while (frontier.Count > 0);

            blobs.Add(blob);
        }

        private List<Vector2> GetNeighbours(Vector2 point)
        {
            var neighbours = new List<Vector2>();

            for (var i = -1; i <= 1; i+=1)
                for (var j = -1; j <= 1; j += 1)
                {
                    var neighbour = new Vector2(i, j) + point;

                    if (neighbour.X < 0 || neighbour.Y < 0 || neighbour.X >= width || neighbour.Y >= height) continue;

                    if (visited.Contains(neighbour)) continue;
                    visited.Add(neighbour);

                    var color = image[(int)neighbour.X + (int)neighbour.Y * width];
                    if (color.Distance(blobColor) > threshold) continue;
                    
                    neighbours.Add(neighbour);
                }
            return neighbours;
        } 
    }
}
