﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Segmenting
{
    public class RegionMerger : IDetectionStep<List<List<Vector2>>>
    {
        private float maxDistance;
        private float colorThreshold;
        private List<List<Vector2>> regions;

        private int width;
         
        public RegionMerger(List<List<Vector2>> regions, float maxDistance, float colorThreshold)
        {
            this.regions = regions.Where(r => r.Count >= 100 && r.Count <= 7500).ToList();
            this.maxDistance = maxDistance;
            this.colorThreshold = colorThreshold;
        }

        public List<List<Vector2>> Calculate(Color[] imageData, int width, int height)
        {
            this.width = width;

            for (var i = 0; i < regions.Count; ++i)
            {
                var region = regions[i];
                for (var j = i; j < regions.Count; ++j)
                {
                    if (i == j) continue;

                    var otherRegion = regions[j];
                    int aIndex, bIndex;
                    float minDistance;

                    GetClosest(region, otherRegion, out aIndex, out bIndex, out minDistance);

                    if (minDistance > maxDistance) continue;

                    var aColor = imageData.At(region[aIndex], width);
                    var bColor = imageData.At(otherRegion[bIndex], width);

                    if (aColor.Distance(bColor) / 255 > colorThreshold) continue;

                    regions.RemoveAt(j);
                    j --;

                    region.AddRange(otherRegion);
                }
            }
            return regions;
        }

        private void GetClosest(List<Vector2> a, List<Vector2> b, out int aIndex, out int bIndex, out float minDistance)
        {
            aIndex = 0;
            bIndex = 0;

            minDistance = -1;

            for (int i = 0; i < a.Count; i++)
            {
                var first = a[i];
                for (int j = 0; j < b.Count; j++)
                {
                    var second = b[j];
                    var dist = Vector2.DistanceSquared(first, second);
                    if (dist < minDistance || minDistance == -1)
                    {
                        aIndex = i;
                        bIndex = j;
                        minDistance = dist;
                    }
                }
            }
        }

        public Color[] Render(Color[] original, List<List<Vector2>> result)
        {
            foreach (var region in result)
            {
                var color = region.Count < 100 || region.Count > 5000 ? Color.Transparent : ColorExtensions.RandomColor();
                foreach (var point in region)
                {
                    original[(int)(point.X + point.Y * width)] = color;
                }
            }

            return original;
        }
    }
}
