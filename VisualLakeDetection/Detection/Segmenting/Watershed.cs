﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Segmenting
{
    public class Watershed
    {
        private Color[] image;
        private int width;
        private int height;

        private int[] regions;
        private List<List<Vector2>> regionPoints; 

        public Watershed(Color[] image, int width, int height)
        {
            this.image = image;
            this.width = width;
            this.height = height;

            regions = new int[image.Length];
        }

        public List<List<Vector2>> Segment()
        {
            if (regionPoints != null) return regionPoints;

            var borders = new List<HashSet<Vector2>>();

            for (var grey = 0; grey < 256; ++grey)
            {
                var acceptable = Acceptable(grey);
                for (int i = 0; i < acceptable.Count; i++)
                {
                    var point = acceptable[i];
                    var validBorders = borders.Where(b => b.Contains(point)).ToList();
                    var isEdge = validBorders.Count > 1;
                    //Add a new region
                    if (!isEdge)
                    {
                        if (validBorders.Count == 0)
                        {
                            var region = new HashSet<Vector2>();
                            borders.Add(region);
                            validBorders.Add(region);
                        }

                        var index = borders.IndexOf(validBorders[0]);
                        regions[(int) (point.X + point.Y*width)] = index + 1;
                        NewNear(point).ForEach(p => validBorders[0].Add(p));
                    }
                    else
                    {
                        regions[(int) (point.X + point.Y*width)] = -1;
                    }
                }
            }

            regionPoints = (from set in borders select set.ToList()).ToList();
            return regionPoints;
        }

        private List<Vector2> Acceptable(int grey)
        {
            var result = new List<Vector2>();
            for (var x = 0; x < width; ++x)
            {
                for (var y = 0; y < height; ++y)
                {
                    var color = image[x + y*width];
                    var greyness = color.ToGreyScale();
                    if (greyness.R == grey) result.Add(new Vector2(x, y));
                }
            }
            return result;
        }

        private List<Vector2> NewNear(Vector2 point)
        {
            var result = new List<Vector2>();

            for (var i = -1; i <= 1; ++i)
                for (var j = -1; j <= 1; ++j)
                {
                    var x = (int) (point.X + i);
                    var y = (int) (point.Y + j);

                    if (x < 0 || y < 0 || x >= width || y >= height) continue;;

                    if (regions[x + y*width] == 0) result.Add(new Vector2(x, y));
                }
            return result;
        }

        public Color[] GetResult()
        {
            var colorOptions = (from region in regions select ColorExtensions.RandomColor()).ToList();

            var colors = new Color[width*height];
            for (var i = 0; i < colors.Length; ++i)
            {
                if (regions[i] == -1) colors[i] = Color.Black;
                else
                {
                    if (regions[i] == 0) colors[i] = Color.Transparent;
                    else colors[i] = Color.Lerp(Color.Black, colorOptions[regions[i] - 1], ((regions[i] + 1.0f)/regionPoints.Count));
                }
            }
            return colors;
        }

    }
}
