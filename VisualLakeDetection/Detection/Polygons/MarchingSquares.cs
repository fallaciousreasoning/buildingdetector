﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection
{
    public class MarchingSquares
    {
        private Color[] image;
        private int width;
        private int height;

        private Vector2 startingPoint;
        private float tolerance;

        private List<Vector2> contour; 

        public MarchingSquares(Color[] image, int width, int height, Vector2 startingPoint, float tolerance)
        {
            this.image = image;
            this.width = width;
            this.height = height;

            this.startingPoint = startingPoint;
            this.tolerance = tolerance;
        }

        public List<Vector2> GetContour()
        {
            if (contour != null)
            {
                return contour;
            }

            contour = new List<Vector2>();

            var current = startingPoint;
            var step = new Vector2();
            var previousStep = new Vector2();

            var closed = false;

            while (!closed)
            {
                var squareValue = GetSquareValue(current);
                switch (squareValue)
                {
                    /**
                    |1| |   |1| |   |1| |
                    | | |   |4| |   |4|8|
                    **/
                    case 1:
                    case 5:
                    case 13:
                        step = new Vector2(0, -1);
                        break;
                    /**
                    | | |   | |2|   |1|2|
                    | |8|   | |8|   | |8|
                    **/
                    case 8:
                    case 10:
                    case 11:
                        step = new Vector2(0, 1);
                        break;
                    /**
                    | | |   | | |   | |2|
                    |4| |   |4|8|   |4|8|
                    **/
                    case 4:
                    case 12:
                    case 14:
                        step = new Vector2(-1, 0);
                        break;
                    /**
                    | |2|   |1|2|   |1|2|
                    | | |   | | |   |4| |
                    **/
                    case 2:
                    case 3:
                    case 7:
                        step = new Vector2(1, 0);
                        break;
                    /**
                    Saddle case 1
                    | |2|
                    |4| |
                    **/
                    case 6:
                        if (previousStep.X == 0 && previousStep.Y == -1)
                        {
                            step = new Vector2(-1, 0);
                        }
                        else
                        {
                            step = new Vector2(1, 0);
                        }
                        break;
                    /**
                    |1| |
                    | |8|
                    **/
                    case 9:
                        if (previousStep.X == 1 && previousStep.Y == 0)
                        {
                            step = new Vector2(0, -1);
                        }
                        else
                        {
                            step = new Vector2(0, 1);
                        }
                        break;
                }
                current += step;
                contour.Add(current);

                previousStep = step;

                if (current == startingPoint)
                    closed = true;
            }

            return contour;
        }

        public Color[] GetResult(Color color) { return GetResult(new Color[width*height], color);}

        public Color[] GetResult(Color[] colors, Color color)
        {
            foreach (var point in contour)
            {
                if (point.X < 0 || point.Y < 0 || point.X >= width || point.Y >= height) continue;
                colors[(int) point.X + (int) point.Y * width] = color;
            }

            return colors;
        }

        private int GetSquareValue(Vector2 point)
        {
            /*
                Squares are as follows:
                |1|2|
                |4|8|
                where the number indicates the tile is solid and
                can be decomposoded to tell which tiles are solid.
            */
            var squareValue = 0;

            if (SolidAt(point - new Vector2(1)))
            {
                squareValue += 1;
            }

            if (SolidAt(point - new Vector2(0, 1)))
            {
                squareValue += 2;
            }

            if (SolidAt(point - new Vector2(1, 0)))
            {
                squareValue += 4;
            }

            if (SolidAt(point))
            {
                squareValue += 8;
            }

            return squareValue;
        }

        private bool SolidAt(Vector2 point)
        {
            if (point.X < 0 || point.Y < 0 || point.X >= width || point.Y >= height) return false;

            return image[(int) point.X + (int) point.Y * width].A / 255.0f > (1 - tolerance);
        }
    }
}
