﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection
{
    public class RamerDouglasPeucker
    {
        public static List<Vector2> Simplify(List<Vector2> input, float epsilon)
        {
            if (input.Count <= 3)
            {
                return input;
            }

            var start = input[0];
            var end = input[input.Count - 1];

            var maxIndex = 0;
            var max = 0f;

            for (var i = 1; i < input.Count - 1; i++)
            {
                var distance = PerpendicularDistance(input[i], start, end);
                if (distance > max)
                {
                    max = distance;
                    maxIndex = i;
                }
            }

            if (max > epsilon)
            {
                var left = input.To(maxIndex);
                var right = input.From(maxIndex);

                left = Simplify(left, epsilon);
                right = Simplify(right, epsilon);

                var result = new List<Vector2>();
                result.AddRange(left);
                result.AddRange(right);
                return result;
            }

            return new List<Vector2>() {start, end};
        }

        public static float PerpendicularDistance(Vector2 point, Vector2 start, Vector2 end)
        {
            if (start.X == end.X) return Math.Abs(point.X - start.X);

            var slope = (end.Y - start.Y)/(end.X - start.X);
            var intercept = start.Y - slope*start.X;

            var numerator = Math.Abs(slope*point.X - point.Y + intercept);
            var denominator = (float)Math.Sqrt(slope*slope + 1);

            return numerator/denominator;
        }
    }
}
