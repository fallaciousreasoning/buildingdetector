﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection
{
    public interface IDetectionStep<T>
    {
        T Calculate(Color[] imageData, int width, int height);
        Color[] Render(Color[] original, T result);
    }
}
