﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Edges
{
    public class BasicEdgeDetection : IDetectionStep<List<Vector2>>
    {
        private int width;
        private float threshold;

        public BasicEdgeDetection(float threshold)
        {
            this.threshold = threshold;
        }

        public List<Vector2> Calculate(Color[] imageData, int width, int height)
        {
            this.width = width;

            var result = new List<Vector2>();

            for (var x = 0; x < width; ++x)
                for (var y = 0; y < height; ++y)
                {
                    var currentColor = imageData[x + y * width];

                    for (var i = 0; i <= 1; ++i)
                        for (var j = 0; j <= 1; ++j)
                        {
                            var point = new Vector2(x + i, y + j);
                            if (i == 0 && j == 0) continue;
                            if (point.X < 0 || point.Y < 0 || point.X >= width || point.Y >= height) continue;

                            var otherColor = imageData[(x + i) + (y + j) * width];
                            if (otherColor.Distance(currentColor) / 255 > threshold)
                            {
                                result.Add(new Vector2(x, y));

                                //Break...
                                i = 7;
                                j = 7;
                            }
                        }
                }
            return result;
        }

        public Color[] Render(Color[] original, List<Vector2> result)
        {
            //var data = new Color[original.Length];
            foreach (var point in result)
            {
                original[(int)(point.X + point.Y * width)] = Color.Black;
            }
            return original;
        }
    }
}
