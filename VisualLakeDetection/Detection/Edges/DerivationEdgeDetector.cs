﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Edges
{
    public class DerivationEdgeDetector : IDetectionStep<List<Vector2>>
    {
        private int width;
        private float threshold;

        public DerivationEdgeDetector(float threshold)
        {
            this.threshold = threshold;
        }

        public List<Vector2> Calculate(Color[] imageData, int width, int height)
        {
            this.width = width;

            var result = new List<Vector2>();
            for (int i = 1; i < width - 1; i++)
            {
                for (int j = 1; j < height - 1; j++)
                {
                    Color cr = imageData.At(i + 1, j, width);
                    Color cl = imageData.At(i - 1, j, width);
                    Color cu = imageData.At(i, j - 1, width);
                    Color cd = imageData.At(i, j + 1, width);
                    int dx = cr.R - cl.R;
                    int dy = cd.R - cu.R;
                    double power = Math.Sqrt(dx * dx / 4 + dy * dy / 4);
                    if (power > 14)
                        result.Add(new Vector2(i, j));
                }
            }
            return result;
        }

        public Color[] Render(Color[] original, List<Vector2> result)
        {
            //var data = new Color[original.Length];
            foreach (var point in result)
            {
                original[(int)(point.X + point.Y * width)] = Color.Black;
            }
            return original;
        }
    }
}
