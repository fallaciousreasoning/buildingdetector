﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Edges
{
    public class KrischEdgeDetector : IDetectionStep<List<Vector2>>
    {
        private static readonly List<int[,]> templates = new List<int[,]>
        {
            new int[,] {{-3, -3, 5}, {-3, 0, 5}, {-3, -3, 5}},
            new int[,] {{-3, 5, 5}, {-3, 0, 5}, {-3, -3, -3}},
            new int[,] {{5, 5, 5}, {-3, 0, -3}, {-3, -3, -3}},
            new int[,] {{5, 5, -3}, {5, 0, -3}, {-3, -3, -3}},
            new int[,] {{5, -3, -3}, {5, 0, -3}, {5, -3, -3}},
            new int[,] {{-3, -3, -3}, {5, 0, -3}, {5, 5, -3}},
            new int[,] {{-3, -3, -3}, {-3, 0, -3}, {5, 5, 5}},
            new int[,] {{-3, -3, -3}, {-3, 0, 5}, {-3, 5, 5}}
        };

        private float width;
        private float threshold;

        public KrischEdgeDetector(float threshold)
        {
            this.threshold = threshold;
        }

        public List<Vector2> Calculate(Color[] imageData, int width, int height)
        {
            this.width = width;

            var edgePoints = new List<Vector2>();

            for (var i = 1; i < width - 1; ++i)
                for (var j = 1; j < height - 1; ++j)
                {
                    Color cr = imageData.At(i + 1, j, width);
                    Color cl = imageData.At(i - 1, j, width);
                    Color cu = imageData.At(i, j - 1, width);
                    Color cd = imageData.At(i, j + 1, width);
                    Color cld = imageData.At(i - 1, j + 1, width);
                    Color clu = imageData.At(i - 1, j - 1, width);
                    Color crd = imageData.At(i + 1, j + 1, width);
                    Color cru = imageData.At(i + 1, j - 1, width);

                    int power = GetMaxD(cr.R, cl.R, cu.R, cd.R, cld.R, clu.R, cru.R, crd.R);
                    if (power/255f > 1 - threshold)
                        edgePoints.Add(new Vector2(i, j));
                }
        

            return edgePoints;
        }

        private int GetD(int cr, int cl, int cu, int cd, int cld, int clu, int cru, int crd, int[,] matrix)
        {
            return Math.Abs(matrix[0, 0] * clu + matrix[0, 1] * cu + matrix[0, 2] * cru
               + matrix[1, 0] * cl + matrix[1, 2] * cr
                  + matrix[2, 0] * cld + matrix[2, 1] * cd + matrix[2, 2] * crd);
        }

        private int GetMaxD(int cr, int cl, int cu, int cd, int cld, int clu, int cru, int crd)
        {
            int max = int.MinValue;
            for (int i = 0; i < templates.Count; i++)
            {
                int newVal = GetD(cr, cl, cu, cd, cld, clu, cru, crd, templates[i]);
                if (newVal > max)
                    max = newVal;
            }
            return max;
        }

        public Color[] Render(Color[] original, List<Vector2> result)
        {
            //var data = new Color[original.Length];
            foreach (var point in result)
            {
                original[(int)(point.X + point.Y * width)] = Color.Black;
            }
            return original;
        }
    }
}
