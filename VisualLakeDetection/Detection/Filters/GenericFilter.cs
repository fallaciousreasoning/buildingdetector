﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Filters
{
    public class GenericFilter : IFilter
    {
        private readonly Func<Color, Color> transform;
        
        public GenericFilter(Func<Color, Color> transform)
        {
            this.transform = transform;
        } 

        public Color[] Filter(Color[] input)
        {
            var result = new Color[input.Length];
            for (var i = 0; i < input.Length; ++i)
            {
                result[i] = transform(input[i]);
            }
            return result;
        }
    }
}
