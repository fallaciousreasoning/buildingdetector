﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Filters
{
    public class GreyScaleFilter :GenericFilter
    {
        public GreyScaleFilter() : base(color =>
        {
            var average = (color.R + color.G + color.B) / 3;
            var newColor = new Color(average, average, average);
            return newColor;
        })
        {
        }
    }
}
