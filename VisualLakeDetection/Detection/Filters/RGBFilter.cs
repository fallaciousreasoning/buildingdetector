﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Filters
{
    public class RGBFilter :GenericFilter
    {
        public RGBFilter() : base(color =>
        {
            var max = new [] {color.R, color.G, color.B}.Max();

            if (color.R == max) return Color.Red;
            if (color.G == max) return new Color(0, 255, 0);
            return new Color(0, 0, 255);
        })
        {
        }
    }
}
