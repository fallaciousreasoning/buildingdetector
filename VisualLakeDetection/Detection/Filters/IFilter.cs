﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace VisualLakeDetection.Detection.Filters
{
    public interface IFilter
    {
        Color[] Filter(Color[] input);
    }
}
