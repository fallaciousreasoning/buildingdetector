﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AccordTest
{
    public partial class FoundDisplay : Form
    {
        public FoundDisplay()
        {
            InitializeComponent();
            KeyPress += (sender, args) =>
            {
                if (args.KeyChar == ' ') Close();
            };
            Shown += (sender, args) => Focus();
        }

        public void SetImage(Bitmap image)
        {
            foundPic.Image = image;
        }

        public void DrawRect(Rectangle rectangle)
        {
            using (var graphics = Graphics.FromImage(foundPic.Image))
            {
                graphics.DrawRectangle(Pens.Red, rectangle);
            }
            foundPic.Invalidate();
        }
    }
}
