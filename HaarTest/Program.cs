﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using AccordTest;
using Emgu.CV;
using Emgu.CV.Structure;

namespace HaarTest
{
    class Program
    {
        private static CascadeClassifier classifier;

        static void Main(string[] args)
        {
            classifier = new CascadeClassifier("training 3/cascade.xml");
            Test();

            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        private static void Test()
        {
            var files = Directory.GetFiles("ManualTest", "*.png");
            var foundDisplay = new FoundDisplay();

            foreach (var file in files)
            {
                var image = Load(file);
                var results = classifier.DetectMultiScale(image, 1.25, 3, new Size(50, 50), new Size(500, 500));

                if (results.Length == 0) continue;

                foundDisplay.SetImage(new Bitmap(file));
                foreach (var rect in results)
                {
                    foundDisplay.DrawRect(rect);
                }

                foundDisplay.ShowDialog();
            }
            foundDisplay.Close();
        }

        private static Image<Gray, byte> Load(string path)
        {
            var image = new Image<Bgr, byte>(new System.Drawing.Bitmap(path));
            var grey = image.Convert<Gray, byte>();
            return grey;
        } 
    }
}
