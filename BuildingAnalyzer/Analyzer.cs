﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Neuro.Networks;
using AccordTest;
using BuildingAnalyzer.Polygons;
using Emgu.CV;
using Emgu.CV.Structure;

namespace BuildingAnalyzer
{
    public class Analyzer : IDisposable
    {
        private readonly CascadeClassifier classifier;
        private readonly DeepBeliefNetwork network;
        private readonly Polygon polygon;
        private readonly Tester tester;

        private List<Bitmap> tiles;
        private List<Image<Gray, byte>> imgs; 

        public Analyzer(DeepBeliefNetwork network, CascadeClassifier classifier, Polygon polygon)
        {
            this.network = network;
            this.polygon = polygon;
            this.classifier = classifier;

            imgs = new List<Image<Gray, byte>>();

            tester = new Tester(network, 10, 1.25, 24, 100);
        }

        public async Task<bool> ContainsBuilding()
        {
            tiles = await PolygonHelpers.GetTiles(polygon, 18);
            return await Task.Run(() => tiles.Any(t => classifier.DetectMultiScale(Load(t), 1.25, 5, new Size(50, 50), new Size(500, 500)).Length > 0)
            );
        }

        private Image<Gray, byte> Load(Bitmap bitmap)
        {
            using (var image = new Image<Bgr, byte>(bitmap))
            {
                var grey = image.Convert<Gray, byte>();
                imgs.Add(grey);
                return grey;
            }
        }

        public void Dispose()
        {
            tiles?.ForEach(t => t.Dispose());
            imgs?.ForEach(t => t.Dispose());
        }
    }
}
