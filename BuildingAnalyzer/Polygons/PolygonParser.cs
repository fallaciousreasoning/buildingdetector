﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace BuildingAnalyzer.Polygons
{
    public class PolygonFileParser : IDisposable
    {
        private readonly string filename;
        private readonly TextReader reader;

        public PolygonFileParser(string filename)
        {
            this.filename = filename;

            reader = new StreamReader(File.OpenRead(filename));
        }

        public static List<Polygon> ParseCoordinateString(string input)
        {
            var polygonList = JsonConvert.DeserializeObject<List<List<List<double>>>>(input);
            var polygons = new List<Polygon>();

            foreach (var pointList in polygonList)
            {
                var points = new List<Vector2>();
                foreach (var coordList in pointList)
                {
                    var v = new Vector2(coordList[1], coordList[0]);
                    points.Add(v);
                }
                polygons.Add(new Polygon(points));
            }

            return polygons;
        }

        public List<Polygon> ReadPolygons()
        {
            var polygons = new List<Polygon>();

            string line;

            while ((line = reader.ReadLine()) != null)
            {
                var polygon = new List<Vector2>();

                var points = line.Split(' ');
                foreach (var point in points)
                {
                    var coords = point.Split(',');
                    double x, y;
                    if (!double.TryParse(coords[0], out x) || !double.TryParse(coords[1], out y)) throw new Exception("Invalid File!");
                    polygon.Add(new Vector2(x, y));
                }

                polygons.Add(new Polygon(polygon));
            }

            return polygons;
        }

        public void Dispose()
        {
            reader.Dispose();
        }
    }
}
