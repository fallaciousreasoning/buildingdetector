﻿using System.Collections.Generic;
using System.Linq;

namespace BuildingAnalyzer.Polygons
{
    public class Polygon
    {
        public Vector2 Min
        {
            get { return min; }
        }

        public Vector2 Max
        {
            get { return max; }
        }

        public Vector2 Centroid { get; private set; }

        public List<Vector2> Points { get; private set; }

        private Vector2 min;
        private Vector2 max;

        public Polygon(List<Vector2> points)
        {
            Points = points;
            Centroid = RecalculateCentroid();
            ComputeMinMax();
        }

        public void ComputeMinMax()
        {
            min = new Vector2(double.MaxValue);
            max = new Vector2();

            foreach (var point in Points)
            {
                if (Min.X > point.X) min.X = point.X;
                if (Min.Y > point.Y) min.Y = point.Y;
                if (Max.X < point.X) max.X = point.X;
                if (Max.Y < point.Y) max.Y = point.Y;
            }
        }

        public bool Contains(Vector2 point)
        {
            if (point.X < min.X || point.Y < min.Y || point.X > max.X || point.Y > max.Y) return false;
            
            var contains = false;
            for (int i = 0, j = Points.Count - 1; i < Points.Count; j = i++)
            {
                if (((Points[i].Y > point.Y) != (Points[j].Y > point.Y)) &&
                    (point.X <
                     (Points[j].X - Points[i].X)*(point.Y - Points[i].Y)/(Points[j].Y - Points[i].Y) + Points[i].X))
                {
                    contains = !contains;
                }
            }
            return contains;
        }

        public Vector2 RecalculateCentroid()
        {
            var sum = Points.Aggregate(new Vector2(), (v1, v2) => v1 + v2);
            return sum/ Points.Count;
        }

        public Polygon ToSlippyPolygon(int zoom=18)
        {
            var slippyPoints = PolygonHelpers.ToSlippyCoords(Points, zoom);
            return new Polygon(slippyPoints);
        }
    }
}
