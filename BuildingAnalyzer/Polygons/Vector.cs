﻿using System;

namespace BuildingAnalyzer.Polygons
{
    public struct Vector2
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Vector2(double n)
        {
            X = n;
            Y = n;
        }

        public Vector2(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double LengthSquared()
        {
            return X*X + Y*Y;
        }

        public double Length()
        {
            return (double)Math.Sqrt(LengthSquared());
        }

        public void Normalize()
        {
            var length = Length();
            X /= length;
            Y /= length;
        }

        public double Dot(Vector2 with)
        {
            return X*with.X + Y*with.Y;
        }

        public double Cross(Vector2 with)
        {
            return X*with.Y - Y * with.X;
        }

        public static Vector2 Normalized(Vector2 vector)
        {
            return vector/vector.Length();
        }

        public static double Distance(Vector2 a, Vector2 b)
        {
            return (a - b).Length();
        }

        public bool IsZero()
        {
            return X < double.Epsilon && Y < double.Epsilon;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector2)) return false;

            var other = (Vector2)obj;
            return other.X == X && other.Y == Y;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() + Y.GetHashCode();
        }

        public override string ToString()
        {
            return $"{{X: {X}, Y: {Y}}}";
        }

        public static Vector2 operator +(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X + b.X, a.Y + b.Y);
        }

        public static Vector2 operator -(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X - b.X, a.Y - b.Y);
        }

        public static Vector2 operator *(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X * b.X, a.Y * b.Y);
        }

        public static Vector2 operator *(Vector2 a, double b)
        {
            return new Vector2(a.X * b, a.Y * b);
        }

        public static Vector2 operator /(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X / b.X, a.Y / b.Y);
        }

        public static Vector2 operator /(Vector2 a, double b)
        {
            return new Vector2(a.X / b, a.Y / b);
        }

        public static bool operator ==(Vector2 a, Vector2 b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Vector2 a, Vector2 b)
        {
            return !(a == b);
        }
    }
}
