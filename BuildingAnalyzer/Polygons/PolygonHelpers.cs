﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using AccordTest;

namespace BuildingAnalyzer.Polygons
{
    public static class PolygonHelpers
    {
        public static List<Polygon> ToSlippyPolygons(List<Polygon> polygons, int zoom = 18)
        {
            var result = from polygon in polygons select polygon.ToSlippyPolygon();
            return result.ToList();
        } 

        public static List<Vector2> ToSlippyCoords(List<Vector2> latLngs, int zoom = 18)
        {
            var points = latLngs.Select(vector => ToSlippyCoord(vector, zoom)).ToList();
            return points;
        }

        public static Vector2 ToSlippyCoord(Vector2 coord, int zoom=18)
        {
            var rho = Math.PI/180.0;
            var lngRad = coord.X*rho;
            var latRad = coord.Y*rho;

            var n = Math.Pow(2, zoom);


            return new Vector2((coord.X + 180)/360.0, (1 - Math.Log(Math.Tan(latRad) + 1/Math.Cos(latRad))/Math.PI)/2f) * n;
        }

        public static List<Vector2> RequiredTiles(Polygon polygon)
        {
            var tiles = new List<Vector2>();

            for (int y = (int) Math.Floor(polygon.Min.Y); y < Math.Ceiling(polygon.Max.Y); ++y)
            {
                for (int x = (int) Math.Floor(polygon.Min.X); x < Math.Ceiling(polygon.Max.X); ++x)
                {
                    tiles.Add(new Vector2(x, y));
                }
            }

            return tiles;
        }

        public static async Task<List<Bitmap>> GetTiles(Polygon polygon, int minZoom = 17)
        {
            var coords = RequiredTiles(polygon.ToSlippyPolygon(minZoom));
            var tiles = await GetTiles(coords, minZoom);
            tiles = Blacken(tiles, polygon.ToSlippyPolygon(minZoom));
            return tiles;
        }

        public static async Task<List<Bitmap>> GetTiles(List<Vector2> coords, int zoom)
        {
            const string googleUrl = "http://mt0.google.com/vt/lyrs=s&x={0}&y={1}&z={2}";
            var tiles = new List<Bitmap>();
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("User-Agent",
                "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");

            foreach (var coord in coords)
            {
                var url = string.Format(googleUrl, (int) coord.X, (int) coord.Y, zoom);
                var response = await client.GetAsync(new Uri(url));

                response.EnsureSuccessStatusCode();

                var stream = await response.Content.ReadAsStreamAsync();
                tiles.Add(new Bitmap(stream));
            }

            return tiles;
        }

        private static List<Bitmap> Blacken(List<Bitmap> tiles, Polygon polygon)
        {
            var widthInTiles = (int)(Math.Ceiling(polygon.Max.X) - Math.Floor(polygon.Min.X));
            var heightInTiles = tiles.Count/widthInTiles;

            var blackened = new List<Bitmap>();

            for (int i = 0; i < widthInTiles; i++)
            {
                for (int j = 0; j < heightInTiles; j++)
                {
                    var bitmap = tiles[i + j * widthInTiles];
                    var pixels = bitmap.GetPixelData();

                    for (var x = 0; x < bitmap.Width; ++x)
                    {
                        for (var y = 0; y < bitmap.Height; ++y)
                        {
                            var actualCoord = ToPolygonCoords(i, j, polygon, new Vector2(x, y));
                            if (!polygon.Contains(actualCoord))
                            {
                                var index = (x + y*bitmap.Width)*3;
                                pixels[index] = 0;
                                pixels[index + 1] = 0;
                                pixels[index + 2] = 0;
                            }
                        }
                    }
                    blackened.Add(pixels.GetBitmapFromRgb(bitmap.Width, bitmap.Height));
                }
            }

            return blackened;
        }

        private static Vector2 ToPolygonCoords(int imageX, int imageY, Polygon polygon, Vector2 point)
        {
            var minX = Math.Floor(polygon.Min.X);
            var minY = Math.Floor(polygon.Min.Y);

            return new Vector2(minX, minY) + new Vector2(imageX, imageY) + point/255;
        }
    }
}
