﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Neuro.Networks;
using BuildingAnalyzer.Polygons;
using Emgu.CV;

namespace BuildingAnalyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var neuralNet = DeepBeliefNetwork.Load("buildingdetector.nn");
            var classifier = new CascadeClassifier("training3/cascade.xml");
            

            Console.WriteLine("Enter MultiPolygon: ");
            var input = "[[[-43.59691197277263, 172.7300387620926], [-43.59759572184234, 172.72733509540555], [-43.599149668105625, 172.72912681102753], [-43.59818622614995, 172.73084342479706], [-43.59691197277263, 172.7300387620926]],[[-43.589222,172.701019],[-43.589542,172.702682],[-43.59052,172.703349],[-43.590955,172.703387],[-43.590972,172.703404],[-43.591081,172.70421],[-43.591023,172.70443],[-43.590835,172.703626],[-43.590467,172.703591],[-43.589352,172.702709],[-43.589059,172.701199],[-43.588767,172.701149],[-43.58812,172.701383],[-43.587169,172.70095],[-43.58659,172.700549],[-43.586495,172.699924],[-43.586945,172.699955],[-43.586931,172.70049],[-43.587331,172.700764],[-43.588105,172.701125],[-43.58872,172.700906],[-43.589222,172.701019]],[[-43.590171,172.696648],[-43.58998,172.696989],[-43.589446,172.696155],[-43.588489,172.696288],[-43.587286,172.697267],[-43.586764,172.697881],[-43.585065,172.69876],[-43.584458,172.699243],[-43.583171,172.699787],[-43.579471,172.700204],[-43.578959,172.700075],[-43.579539,172.698925],[-43.580193,172.698927],[-43.581279,172.696692],[-43.581504,172.696672],[-43.582404,172.69662],[-43.582087,172.695581],[-43.581681,172.695375],[-43.58077,172.695517],[-43.580769,172.695494],[-43.579639,172.695403],[-43.57964,172.695381],[-43.580767,172.695472],[-43.581678,172.69533],[-43.581417,172.691935],[-43.589775,172.690574],[-43.589985,172.690903],[-43.59042,172.691842],[-43.590315,172.692924],[-43.590465,172.694029],[-43.59068,172.694538],[-43.590559,172.695296],[-43.590579,172.696026],[-43.590171,172.696648]]]";
            var polygons = PolygonFileParser.ParseCoordinateString(input);
            for (int i = 0; i < polygons.Count; i++)
            {
                var polygon = polygons[i];
                using (var analyzer = new Analyzer(neuralNet, classifier, polygon))
                {
                    var tilesTask = analyzer.ContainsBuilding();
                    tilesTask.ConfigureAwait(false);
                    tilesTask.Wait();

                    var hasBuilding = tilesTask.Result;
                    Console.WriteLine(hasBuilding
                        ? $"Polygon {i} has a building"
                        : $"Polygon {i} does not contain a building");
                }
            }

            Console.WriteLine("Done");
            Console.ReadKey();
        }
    }
}
