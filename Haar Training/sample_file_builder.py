positive_folder = "Positives"
negative_folder = "High Resolution Negatives"

positive_file = "positives.txt"
negative_file = "negatives.txt"

negative_count = -1
positive_count = -1

from os import listdir

with open(positive_file, 'w') as f:
	files = listdir(positive_folder)
	for image in files[:positive_count]:
		f.write(positive_folder + "\\" + image + "\n")
	#positive_count = len(files) if positive_count == -1 else max(positive_count, len(files)):

#print(positive_count)

with open(negative_file, 'w') as f:
	files = listdir(negative_folder)
	for image in files[:negative_count]:
		f.write(negative_folder + "\\" + image + "\n")
