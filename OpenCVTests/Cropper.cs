﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ImageProcessor;
using OpenCVTests.Parsing;
using OpenCVTests.Polygons;

namespace OpenCVTests
{
    public class Cropper
    {
        private const string CROPPED_FOLDER = "Positives";

        private const int IMAGE_SIZE = 256;

        private readonly Queue<Polygon> cropQueue = new Queue<Polygon>();
         
        private readonly List<Polygon> polygons;
        private readonly TextWriter outputWriter;
        private readonly int poolSize;

        private int totalCount;
        private int doneCount;

        private int tolerance;
        
        public Cropper(List<Polygon> polygons, TextWriter outputWriter=null, int poolSize = 10, int tolerance = 5)
        {
            this.tolerance = tolerance;
            this.polygons = polygons;
            this.outputWriter = outputWriter ?? new StreamWriter(Stream.Null);
            this.poolSize = poolSize;
            if (!Directory.Exists(CROPPED_FOLDER)) Directory.CreateDirectory(CROPPED_FOLDER);
        }

        public void Start()
        {
            cropQueue.Clear();

            doneCount = 0;
            totalCount = polygons.Count;

            polygons.ForEach(cropQueue.Enqueue);

            for (var i = 0; i < poolSize; ++i) BeginTask();
        }

        private void BeginTask()
        {
            if (cropQueue.Count == 0) return;

            var polygon = cropQueue.Dequeue();
            Task.Run(() => CropPolygon(polygon));
        }

        private void Complete(Polygon polygon)
        {
            doneCount++;
            outputWriter.WriteLine($"{(float)doneCount/totalCount * 100}% of polygons cropped!");
            BeginTask();
        }

        private void Fail(Polygon polygon)
        {
            BeginTask();
        }

        private void CropPolygon(Polygon polygon)
        {
            var tilePoints = PolygonHelpers.RequiredTiles(polygon);
            tilePoints.Sort((v1, v2) =>
            {
                if (v1.Y == v2.Y) return Comparer.Default.Compare(v1.X, v2.X);
                return Comparer.Default.Compare(v1.Y, v2.Y);
            });

            var widthTiles = (int) (Math.Ceiling(polygon.Max.X) - Math.Floor(polygon.Min.X));
            var heightTiles = tilePoints.Count/ widthTiles;

            var result = new byte[widthTiles * heightTiles * IMAGE_SIZE*IMAGE_SIZE*3];
            
            for (var x = 0; x < widthTiles; ++x)
                for (var y = 0; y < heightTiles; ++y)
                {
                    var pos = tilePoints[x + y*widthTiles];
                    var filename = $"Tiles\\{(int)pos.X}, {(int)pos.Y}.png";
                    LoadImageTo(x, y, filename, widthTiles*IMAGE_SIZE, result);
                }

//            foreach (var point in polygon.Points)
//            {
//                var x = (int)((point.X - Math.Floor(polygon.Min.X))/(Math.Ceiling(polygon.Max.X) - Math.Floor(polygon.Min.X)) *widthTiles*IMAGE_SIZE);
//                var y = (int)((point.Y - Math.Floor(polygon.Min.Y)) / (Math.Ceiling(polygon.Max.Y) - Math.Floor(polygon.Min.Y)) *heightTiles* IMAGE_SIZE);
//
//                Color(x, y, widthTiles*IMAGE_SIZE, result);
//            }

            var resultImage = new Bitmap(widthTiles*IMAGE_SIZE, heightTiles*IMAGE_SIZE);
            var data = resultImage.LockBits(new Rectangle(Point.Empty, resultImage.Size), ImageLockMode.WriteOnly,
                PixelFormat.Format24bppRgb);

            Marshal.Copy(result, 0, data.Scan0, result.Length);

            resultImage.UnlockBits(data);

            var fileName = $"{CROPPED_FOLDER}\\" + Guid.NewGuid() + ".png";
            var memoryStream = new MemoryStream();
            resultImage.Save(memoryStream, ImageFormat.Png);

            resultImage.Dispose();
            memoryStream.Close();

            var minX = (int)Math.Max(((polygon.Min.X - Math.Floor(polygon.Min.X)) / (Math.Ceiling(polygon.Max.X) - Math.Floor(polygon.Min.X)) * widthTiles * IMAGE_SIZE) - tolerance, 0);
            var minY = (int)Math.Max(((polygon.Min.Y - Math.Floor(polygon.Min.Y)) / (Math.Ceiling(polygon.Max.Y) - Math.Floor(polygon.Min.Y)) * heightTiles * IMAGE_SIZE) - tolerance, 0);
            var maxX = (int)Math.Min(((polygon.Max.X - Math.Floor(polygon.Min.X)) / (Math.Ceiling(polygon.Max.X) - Math.Floor(polygon.Min.X)) * widthTiles * IMAGE_SIZE) + tolerance, widthTiles*IMAGE_SIZE);
            var maxY = (int)Math.Min(((polygon.Max.Y - Math.Floor(polygon.Min.Y)) / (Math.Ceiling(polygon.Max.Y) - Math.Floor(polygon.Min.Y)) * heightTiles * IMAGE_SIZE) + tolerance, heightTiles*IMAGE_SIZE);

            //Make sure we get a square image
            var width = maxX - minX;
            var height = maxY - minY;
            var centreX = (minX + maxX)/2;
            var centreY = (minY + maxY)/2;

            var size = Math.Max(width, height);
            var pX = Math.Max(0, centreX - size/2);
            var pY = Math.Max(0, centreY - size/2);

            using (var factory = new ImageFactory())
            {
                factory.Load(memoryStream.ToArray())
                    .Crop(new Rectangle(pX, pY, size, size))
                    .Resize(new Size(24, 24))
                    .Save(File.Create(fileName));
            }

            Complete(polygon);
        }

        private void Color(int x, int y, int imageWidth, byte[] data, int dotSize=4)
        {
            for (var i = -dotSize/2; i < dotSize/2; ++i)
                for (var j = -dotSize/2; j < dotSize/2; ++j)
                {
                    var xc = i + x;
                    var yc = j + y;

                    var index = (xc + yc*imageWidth)*3;
                    data[index] = 0;
                    data[index + 1] = 0;
                    data[index + 2] = 255;
                }
        }

        private void LoadImageTo(int xOffset, int yOffset, string filename, int resultWidth, byte[] to)
        {
            var s = new Bitmap(filename);

            var data = s.LockBits(new Rectangle(0, 0, s.Width, s.Height), ImageLockMode.ReadOnly, s.PixelFormat);
            var byteData = new byte[s.Width*s.Height*3];
            Marshal.Copy(data.Scan0, byteData, 0, byteData.Length);

            s.UnlockBits(data);
            s.Dispose();

            for (var x = 0; x < IMAGE_SIZE; x++)
                for (var y = 0; y < IMAGE_SIZE; ++y)
                {
                    var index = (x + y*IMAGE_SIZE)*3;

                    var b = byteData[index];
                    var g = byteData[index + 1];
                    var r = byteData[index + 2];

                    var resultIndex = ((xOffset * IMAGE_SIZE + x) + (yOffset * IMAGE_SIZE + y)*resultWidth)*3;
                    to[resultIndex] = b;
                    to[resultIndex + 1] = g;
                    to[resultIndex + 2] = r;
                }
        }
    }
}
