﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LakeDetection;
using OpenCVTests.Parsing;

namespace OpenCVTests
{
    public partial class NegativeDownloader : Form
    {
        private readonly double minLat;
        private readonly double minLng;
        private readonly double maxLng;
        private readonly double maxLat;

        private const string GOOGLE_MAP_URL = "http://mt0.google.com/vt/lyrs=s&x={0}&y={1}&z=18";
        private static readonly Random random = new Random();

        private Action<Bitmap> callback;

        private HttpClient client = new HttpClient();

        private Bitmap currentBitmap;
        private int need;

        public NegativeDownloader(int need, double minLat, double minLng, double maxLat, double maxLng, Action<Bitmap> callback=null)
        {
            this.minLat = minLat;
            this.minLng = minLng;
            this.maxLng = maxLng;
            this.maxLat = maxLat;
            this.callback = callback ?? ((i) => { });
            this.need = need;

            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");

            InitializeComponent();

            this.KeyPress += (sender, args) =>
            {
                if (args.KeyChar == ' ')
                {
                    this.callback(currentBitmap);
                    need--;
                }
                if (need <= 0) Close();
                else FetchNextImage();
            };

            FetchNextImage();
        }

        private async void FetchNextImage()
        {
            var lat = random.NextDouble()*(maxLat - minLat) + minLat;
            var lng = random.NextDouble()*(maxLng - minLng) + minLng;
            var pos = PolygonHelpers.ToSlippyCoord(new Vector(lng, lat));

            var url = string.Format(GOOGLE_MAP_URL, (int) pos.X, (int) pos.Y);
            var response = await client.GetAsync(url);

//            var save = File.Create("temp.png");
//            await response.Content.CopyToAsync(save);
//            save.Close();

            currentBitmap = new Bitmap(await response.Content.ReadAsStreamAsync());

            currentImage.Image = currentBitmap;
        }
    }
}
