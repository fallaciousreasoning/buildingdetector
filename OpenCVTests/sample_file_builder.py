positive_folder = "Positives"
negative_folder = "Negatives"

positive_file = "positives.txt"
negative_file = "negatives.txt"

from os import listdir

with open(positive_file, 'w') as f:
	files = listdir(positive_folder)
	for image in files:
		f.write(positive_folder + "\\" + image + "\n")

with open(negative_file, 'w') as f:
	files = listdir(negative_folder)
	for image in files:
		f.write(negative_folder + "\\" + image + "\n")