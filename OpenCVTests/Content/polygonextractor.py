import re

filename = 'christchurch-city-building-footprints.kml'
output = 'buildingpoints.csv'

coords = []

with open(filename, 'r') as f:
	text = f.read()

	coord_regex = re.compile(r'<coordinates>(.*?)</coordinates>')
	matches = coord_regex.findall(text)
	for match in matches:
		coords.append(match)

with open(output, 'w') as f:
	for coord in coords[0:10000]:
		f.write(coord.strip() + '\n')

