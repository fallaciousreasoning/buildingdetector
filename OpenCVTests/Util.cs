﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCVTests
{
    public static class Util
    {
        private static readonly Random random = new Random();

        public static double RandomDouble()
        {
            return random.NextDouble();
        }

        public static double Sigmoid(double z)
        {
            return 1.0 / (1.0 + Math.Exp(-z));
        }

        public static double SigmoidPrime(double z)
        {
            return Sigmoid(z)*(1 - Sigmoid(z));
        }
    }
}
