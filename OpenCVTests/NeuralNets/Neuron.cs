﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OpenCVTests.NeuralNets
{
    public class Neuron
    {
        public int Inputs { get; set; }
        public List<double> Weights { get; set; }

        private double output;
        private double activation;
        private double derivative;
        private double error;

        [JsonIgnore]
        internal NeuronLayer Layer { get; set; }

        public Neuron()
        {
            
        }

        public Neuron(int inputs, NeuronLayer layer)
        {
            this.Inputs = inputs;
            this.Layer = layer;

            Weights = new List<double>(inputs + 1);

            //Initialize weights randomly
            for (var i = 0; i < inputs; ++i) Weights.Add(Util.RandomDouble()*2 -1);
        }

        private double CalculateActivation(List<double> input)
        {
            if (input.Count != Inputs) throw new ArgumentException($"Incorrect number of inputs! (expected {Inputs} got {input.Count})");

            var sum = 0.0;
            for (var i = 0; i < input.Count - 1; ++i)
                sum += input[i]*Weights[i];

            //Add the bias
            sum += Weights[Weights.Count - 1];

            return sum;
        }

        public double ForwardPropogateSigmoid(List<double> input)
        {
            error = 0;
            output = CalculateActivation(input);
            activation = Util.Sigmoid(output);
            derivative = Util.SigmoidPrime(output);

            return activation;
        }

        public void CollectError(double delta)
        {
            if (Layer.PreviousLayer == null) return;
            error += delta;
            for (var i = 0; i < Layer.PreviousLayer.Neurons.Count - 1; ++i)
                Layer.PreviousLayer.Neurons[i].CollectError(error * Weights[i]);
        }

        public void AdjustWeights(double learningRate)
        {
            for (var i = 0; i < Weights.Count - 1; ++i)
            {
                Weights[i] += error*derivative*learningRate*Layer.PreviousLayer.Neurons[i].activation;
            }

            Weights[Weights.Count - 1] += error*derivative*learningRate;
        }
    }
}
