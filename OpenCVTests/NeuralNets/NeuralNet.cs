﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OpenCVTests.NeuralNets
{
    public class NeuralNet
    {
        public int Inputs { get; set; }
        public int Outputs { get; set; }

        public List<NeuronLayer> Layers { get; set; }

        public NeuralNet()
        {
            
        }

        public NeuralNet(int inputs, int outputs, List<int> hiddenLayerCounts)
        {
            this.Inputs = inputs;
            this.Outputs = outputs;

            Layers = new List<NeuronLayer>(hiddenLayerCounts.Count + 1);
            for (var i = 0; i < hiddenLayerCounts.Count; ++i)
            {
                var inputCount = i == 0 ? inputs : hiddenLayerCounts[i - 1];
                Layers.Add(new NeuronLayer(hiddenLayerCounts[i], inputCount, i == 0 ? null : Layers[i - 1]));
            }

            //If there are no hidden Layers, the input to our output layer is our number of Inputs
            var outputInputCount = hiddenLayerCounts.Count == 0 ? inputs : hiddenLayerCounts[hiddenLayerCounts.Count - 1];
            var outputLayer = new NeuronLayer(outputs, outputInputCount, Layers[Layers.Count - 1]);
            Layers.Add(outputLayer);
        }

        public List<double> Activate(List<double> input)
        {
            if (input.Count != Inputs) throw new ArgumentException($"Unexpected number of inputs! (expected {Inputs} got {input.Count})");

            var i = 0;
            do
            {
                input = Layers[i++].GetOutput(input);
            } while (i < Layers.Count);

            return input;
        }

        public double Train(double learningRate, List<Pattern> patterns)
        {
            return patterns.Aggregate(0.0,
                (error, pattern) => error + Train(learningRate, pattern.Input, pattern.Output));
        }

        public double Train(double learningRate, List<double> input, List<double> expectedOutput)
        {
            if (input.Count != Inputs) throw new ArgumentException($"Unexpected number of inputs! (expected {Inputs} got {input.Count})");
            if (expectedOutput.Count != Outputs) throw new ArgumentException($"Unexpected number of outputs! (expected {Outputs} got {expectedOutput.Count})");

            var error = 0.0;
            var output = Activate(input);
            for (int i = 0; i < output.Count; i++)
            {
                var value = output[i];
                var expected = expectedOutput[i];

                var delta = expected - value;
                Layers[Layers.Count - 1].Neurons[i].CollectError(delta);
                error += Math.Pow(delta, 2);
            }

            AdjustWeights(learningRate);
            return error;
        }

        private void AdjustWeights(double learningRate)
        {
            for (var i = Layers.Count - 1; i > 0; i-- )
                foreach(var neuron in Layers[i].Neurons)
                    neuron.AdjustWeights(learningRate);
        }

        public static NeuralNet Load(string file)
        {
            NeuralNet net;
            using (StreamReader reader = new StreamReader(File.OpenRead(file)))
            {
                var json = reader.ReadToEnd();
                net = JsonConvert.DeserializeObject<NeuralNet>(json);
            }

            for (var i = 0; i < net.Layers.Count; ++i)
            {
                if (i != 0)
                    net.Layers[i].PreviousLayer = net.Layers[i - 1];
                foreach (var neuron in net.Layers[i].Neurons)
                {
                    neuron.Layer = net.Layers[i];
                }
            }

            return net;
        }

        public static void Save(NeuralNet net, string file)
        {
            using (var writer = new StreamWriter(File.Create(file)))
            {
                var json = JsonConvert.SerializeObject(net);
                writer.Write(json);
            }
        }
    }
}
