﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OpenCVTests.NeuralNets
{
    public class NeuronLayer
    {
        public int NeuronCount { get; set; }
        public int InputCount { get; set; }

        public List<Neuron> Neurons { get; set; }

        [JsonIgnore]
        public NeuronLayer PreviousLayer { get; set; }

        public NeuronLayer()
        {
            
        }

        public NeuronLayer(int neuronCount, int inputCount, NeuronLayer previousLayer)
        {
            this.NeuronCount = neuronCount;
            this.InputCount = inputCount;
            PreviousLayer = previousLayer;

            Neurons = new List<Neuron>(neuronCount);
            for (var i = 0; i < this.NeuronCount; ++i) Neurons.Add(new Neuron(inputCount, this));
        }

        public List<double> GetOutput(List<double> input)
        {
            if (input.Count != InputCount) throw new ArgumentException($"Unexpected number of inputs! (expected {InputCount} got {input.Count})");

            var output = new List<double>();

            foreach (var neuron in Neurons)
                output.Add(neuron.ForwardPropogateSigmoid(input));

            return output;
        }
    }
}
