﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenCVTests.NeuralNets
{
    public class Pattern
    { 
        public List<double> Input { get; set; } 
        public List<double> Output { get; set; } 
    }
}
