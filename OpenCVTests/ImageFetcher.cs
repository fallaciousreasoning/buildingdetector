﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LakeDetection;
using OpenCVTests.Parsing;

namespace OpenCVTests.Polygons
{
    public class ImageFetcher
    {
        private const string GOOGLE_URL = "http://mt0.google.com/vt/lyrs=s&x={0}&y={1}&z=18";
        private const string TILE_DIRECTORY = "Tiles";

        private readonly Queue<Vector> imageRequestQueue = new Queue<Vector>();
        private readonly List<Vector> points;

        private TextWriter outputWriter;

        private int poolSize;

        private int doneCount;
        private int totalCount;

        public ImageFetcher(List<Polygon> polygons, TextWriter outputWriter = null, int poolSize=15)
        {
            this.poolSize = poolSize;

            if (!Directory.Exists(TILE_DIRECTORY)) Directory.CreateDirectory(TILE_DIRECTORY);

            var pointsSet = new HashSet<Vector>();
            foreach (var polygon in polygons)
            {
                PolygonHelpers.RequiredTiles(polygon).ForEach(v => pointsSet.Add(v));
            }
            points = pointsSet.ToList();

            this.outputWriter = outputWriter ?? new StreamWriter(Stream.Null);
        }

        public void Start()
        {
            //Prepare for calculating percentages
            doneCount = 0;
            totalCount = points.Count;

            //Enqueue all the images
            points.ForEach(imageRequestQueue.Enqueue);

            for (var i = 0; i < poolSize; ++i) BeginNext();
        }

        private void BeginNext()
        {
            if (imageRequestQueue.Count <= 0) return;

            var point = imageRequestQueue.Dequeue();
            DownloadImage((int)point.X, (int)point.Y);
        }

        private async Task DownloadImage(int x, int y)
        {
            var fileName = $"Tiles\\{x}, {y}.png";

            //If we've already downloaded the image, we're done!
            if (File.Exists(fileName))
            {
                Complete(x, y);
                return;
            }

            var url = string.Format(GOOGLE_URL, x, y);
            var client = new HttpClient();
//            client.DefaultRequestHeaders.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
//            client.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate, sdch");
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36");

            var response = await client.GetAsync(url);

            //If we failed, retry (and hope...)
            if (!response.IsSuccessStatusCode)
            {
                Fail(x, y);
                return;
            }

            using (var saveFile = File.Create(fileName))
            {
                //Save the image
                await response.Content.CopyToAsync(saveFile);
            }

            Complete(x, y);
        }

        private void Fail(int x, int y)
        {
            outputWriter.WriteLine($"Failed to get Tile for {x}, {y} (requeuing)");
            imageRequestQueue.Enqueue(new Vector(x, y));

            BeginNext();
        }

        private void Complete(int x, int y)
        {
            doneCount++;
            outputWriter.WriteLine($"Downloaded Tile for {x}, {y} ({(float)doneCount/totalCount * 100}% complete)");

            BeginNext();
        }
    }
}
