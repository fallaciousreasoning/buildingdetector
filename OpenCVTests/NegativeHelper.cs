﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageProcessor;

namespace OpenCVTests
{
    public class NegativeHelper
    {
        private string negativesFolder = "Negatives";

        public int Count;
        private int done;
        private NegativeDownloader negativeDownloader;

        private TextWriter outputWriter;

        public NegativeHelper(int count, TextWriter outputWriter=null, string negativesFolder="Negatives")
        {
            this.outputWriter = outputWriter ?? new StreamWriter(Stream.Null);
            this.negativesFolder = negativesFolder;

            Count = count;
            if (!Directory.Exists(negativesFolder)) Directory.CreateDirectory(negativesFolder);
        }

        public void Start()
        {
            negativeDownloader = new NegativeDownloader(Count, -43.339740, 172.196875, -43.495585, 172.296867, SaveImage);
            negativeDownloader.ShowDialog();
        }

        public void SaveImage(Bitmap image)
        {
            using (var stream = new MemoryStream())
            {
                image.Save(stream, ImageFormat.Png);

                var end = "\\" + Guid.NewGuid() + ".png";
                using (var saveStream = File.Create("High Resolution Negatives" + end))
                {
                    image.Save(saveStream, ImageFormat.Png);
                }

                using (var saveStream = File.Create(negativesFolder + end))
                {
                    using (var imageFactory = new ImageFactory())
                    {
                        imageFactory.Load(stream.ToArray()).Resize(new Size(24, 24)).Save(saveStream);
                    }
                }
            }
            done++;
            outputWriter.WriteLine($"{(float)done/Count * 100}% of negatives picked :/");
        }
    }
}
