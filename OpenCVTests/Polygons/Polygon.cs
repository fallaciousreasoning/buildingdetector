﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LakeDetection;
using OpenCVTests.Parsing;

namespace OpenCVTests.Polygons
{
    public class Polygon
    {
        public Vector Min
        {
            get { return min; }
        }

        public Vector Max
        {
            get { return max; }
        }

        public Vector Centroid { get; private set; }

        public List<Vector> Points { get; private set; }

        private Vector min;
        private Vector max;

        public Polygon(List<Vector> points)
        {
            Points = points;
            Centroid = RecalculateCentroid();
            ComputeMinMax();
        }

        public void ComputeMinMax()
        {
            min = new Vector(double.MaxValue);
            max = new Vector();

            foreach (var point in Points)
            {
                if (Min.X > point.X) min.X = point.X;
                if (Min.Y > point.Y) min.Y = point.Y;
                if (Max.X < point.X) max.X = point.X;
                if (Max.Y < point.Y) max.Y = point.Y;
            }
        }

        public Vector RecalculateCentroid()
        {
            var sum = Points.Aggregate(new Vector(), (v1, v2) => v1 + v2);
            return sum/ Points.Count;
        }

        public Polygon ToSlippyPolygon()
        {
            var slippyPoints = PolygonHelpers.ToSlippyCoords(Points);
            return new Polygon(slippyPoints);
        }
    }
}
