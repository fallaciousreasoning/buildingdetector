﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LakeDetection;
using OpenCVTests.Polygons;

namespace OpenCVTests.Parsing
{
    public static class PolygonHelpers
    {
        public static List<Polygon> ToSlippyPolygons(List<Polygon> polygons, int zoom = 18)
        {
            var result = from polygon in polygons select polygon.ToSlippyPolygon();
            return result.ToList();
        } 

        public static List<Vector> ToSlippyCoords(List<Vector> latLngs, int zoom = 18)
        {
            var points = latLngs.Select(vector => ToSlippyCoord(vector, zoom)).ToList();
            return points;
        }

        public static Vector ToSlippyCoord(Vector coord, int zoom=18)
        {
            var rho = Math.PI/180.0;
            var lngRad = coord.X*rho;
            var latRad = coord.Y*rho;

            var n = Math.Pow(2, zoom);


            return new Vector((coord.X + 180)/360, (1 - Math.Log(Math.Tan(latRad) + 1/Math.Cos(latRad))/Math.PI)/2f) * n;
        }

        public static List<Vector> RequiredTiles(Polygon polygon)
        {
            var tiles = new List<Vector>();

            for (int x = (int)Math.Floor(polygon.Min.X); x < Math.Ceiling(polygon.Max.X); ++x)
            {
                for (int y = (int)Math.Floor(polygon.Min.Y); y < Math.Ceiling(polygon.Max.Y); ++y)
                {
                    tiles.Add(new Vector(x, y));
                }
            }

            return tiles;
        } 
    }
}
