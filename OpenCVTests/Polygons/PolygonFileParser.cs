﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LakeDetection;
using OpenCVTests.Polygons;

namespace OpenCVTests.Parsing
{
    public class PolygonFileParser : IDisposable
    {
        private readonly string filename;
        private readonly TextReader reader;

        public PolygonFileParser(string filename)
        {
            this.filename = filename;

            reader = new StreamReader(File.OpenRead(filename));
        }

        public List<Polygon> ReadPolygons()
        {
            var polygons = new List<Polygon>();

            string line;

            while ((line = reader.ReadLine()) != null)
            {
                var polygon = new List<Vector>();

                var points = line.Split(' ');
                foreach (var point in points)
                {
                    var coords = point.Split(',');
                    double x, y;
                    if (!double.TryParse(coords[0], out x) || !double.TryParse(coords[1], out y)) throw new Exception("Invalid File!");
                    polygon.Add(new Vector(x, y));
                }

                polygons.Add(new Polygon(polygon));
            }

            return polygons;
        }

        public void Dispose()
        {
            reader.Dispose();
        }
    }
}
