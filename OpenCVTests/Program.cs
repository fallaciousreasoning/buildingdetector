﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ImageProcessor;
using OpenCVTests.NeuralNets;
using OpenCVTests.Parsing;
using OpenCVTests.Polygons;
using Point = System.Drawing.Point;

namespace OpenCVTests
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Done!");
            Console.ReadKey();
        }

        private static void Test()
        {
            var neuralNet = File.Exists("neuralnet.json") ? NeuralNet.Load("neuralnet.json") : new NeuralNet(24 * 24 * 3, 1, new List<int>() { 10,10,10,10,10 });

            var positiveImages = File.ReadAllLines("positives.txt");
            var negativeImages = File.ReadAllLines("negatives.txt");

            var total = positiveImages.Length + negativeImages.Length;
            var correct = 0;

            foreach (var image in positiveImages)
            {
                var result = neuralNet.Activate(LoadImage(image))[0];
                correct += (int)Math.Round(result);
            }

            foreach (var negativeImage in negativeImages)
            {
                var result = neuralNet.Activate(LoadImage(negativeImage))[0];
                correct += 1 - (int)Math.Round(result);
            }

            Console.WriteLine($"Tested! Got {correct} out of {total} correct ({(float)correct/total*100}% accurate)!");
        }

        private static void Train()
        {
            var neuralNet = new NeuralNet(24 * 24 * 3, 1, new List<int>() { 100 });

            var positiveImages = File.ReadAllLines("positives.txt");
            var negativeImages = File.ReadAllLines("negatives.txt");

            var total = positiveImages.Length + negativeImages.Length;
            var done = 0;

            var learningRate = 0.01;
            var error = 0.0;

            foreach (var image in positiveImages)
            {
                error += neuralNet.Train(learningRate, LoadImage(image), new List<double>() { 1 });
                done++;
                Log(done, total);
            }

            foreach (var negativeImage in negativeImages)
            {
                error += neuralNet.Train(learningRate, LoadImage(negativeImage), new List<double>() { 0 });
                done++;
                Log(done, total);
            }

            Console.WriteLine($"Trained! (current error is {error})");
            NeuralNet.Save(neuralNet, "neuralnet.json");
        }

        private static void Log(int done, int total)
        {
            Console.WriteLine($"Done {done} of {total} ({(float)done/total*100}% complete)!");
        }

        private static List<double> LoadImage(string filename)
        {
            var bitmap = new Bitmap(filename);

            var data = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), ImageLockMode.ReadOnly,
                PixelFormat.Format24bppRgb);

            var result = new byte[24 * 24 * 3];
            Marshal.Copy(data.Scan0, result, 0, result.Length);
            bitmap.UnlockBits(data);

            bitmap.Dispose();

            return (from b in result select (double) b/255).ToList();
        } 
    }
}
